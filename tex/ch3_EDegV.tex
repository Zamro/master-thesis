\chapter{Number of edges}
In this section we will show expected degree of vertices in GIRG and use it to calculate expected number of generated edges.
Through this section we will denote by $X_{uv}$ the event, that vertices $u$ and $v$ are connected. 
As $X_{uv}$ is an indicator variable, we have $\E[X_{uv}] = p_{uv}$.
Additionally, to simplify calculations, we will denote by $\E[X_{vv}]$ expected probability of connecting $v$ with virtual vertex having the same weight $w_v$, but random position.
Firstly we will introduce generalized harmonic numbers and present its asymptotic.
Then we will prove, that:
\begin{lemma}[Probability of connecting two vertices]\ \\
	\label{lem:expectedProbability}
	For $u,v \in \{1, \ldots, n\}, u \neq v$ we have:
	\[
		E[X_{uv}] =
		\begin{cases}
			1 & \text{ for } w_u w_v \geq \frac{W}{2^d}\\
			\frac{\alpha}{\alpha - 1} \frac{2^dw_uw_v}{W} - \frac{1}{\alpha - 1} \left( \frac{2^dw_uw_v}{W} \right) ^{\alpha} & \text{ for } w_u w_v < \frac{W}{2^d}
		\end{cases}
	\]
\end{lemma}
We will use both asymptotic of generalized harmonic numbers and Lemma \ref{lem:expectedProbability} to show, that:
\begin{theorem}[Expected vertex degree]\ \\
	\label{th:expectedDegree}
	For
	$v \in \{1, \ldots, n\}$
	we have
	$\E[\deg(v)] = \frac{\alpha}{\alpha - 1}2^d w_v + o(w_v)$.
\end{theorem}
It is a more detailed version of Lemma 4.4 from \cite{GIRG}, which states, that $\E[\deg(v)] = \Theta(w_v)$.
From this theorem we will derive expected number of edges in graph:
\begin{theorem}[Expected number of edges in graph]\ \\
	\label{th:expectedEdges}
	\[\E[m] = \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n + o(n)\]
\end{theorem}

\section{Asymptotic behaviour of generalized harmonic numbers}
\begin{definition}[Generalized harmonic number]\ \\
	A generalized harmonic number is a number of the form:
	\[H_{n,a} = \sum\limits_{i=1}^{n}\left( \frac{1}{i} \right)^a\]
\end{definition}
Its asymptotic behaviour when $n\rightarrow\infty$ is as follows:
\begin{itemize}
\item for $a = 1$:
	\begin{equation}
		\label{eq:asymptotics1}
		H_{n,a} = H_n = \gamma + \log\left(n\right) + \Theta\left(\frac{1}{n}\right),
	\end{equation}
	where $H_n$ denotes standard harmonic number and $\gamma \approx 0.577$ is Euler gamma,
\item for $a \neq 1$ \cite{zetaShort}\cite{zetaLong}
	\begin{equation}
		\begin{split}
		\label{eq:asymptotics2}
		H_{n,a}
			= {} & \zeta \left(a\right)
				+ n^{-a}\left(
					\frac{n}{1-a}
					+ \frac{1}{2} + \Theta\left (\frac{1}{n}\right)
				\right)\\
			= {} &  \frac{n^{1-a}}{1 - a} + \zeta \left(a\right) + \Theta\left(n^{-a}\right )
		\end{split}
	\end{equation}
	where $\zeta $ denotes Riemann zeta function.
	It is worth noting, that it gives:
		$H_{n,a} = \Theta(1) \text{ for } a > 1$
	and
		$H_{n,a} = \Theta(n^{1-a}) \text{ for } a < 1$.
\end{itemize}

\section{Probability of connecting two vertices.}
We will now prove Lemma \ref{lem:expectedProbability}.
For a given vertex $v$ with fixed values of $x_v$, $w_v$ and any vertex $u$ with a fixed value of $w_u$ and a value of $x_u$ sampled from uniform distribution over $[0,1)^d$, we have:
\[
	\E[X_{uv}]	= \E\left[\min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}\right].
\]
Let us represent $\left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha$ as a random variable $Y$ with probability density function $g$.
We can see that $X_{uv}$ is a mixed random variable with a probability density function given by:
\[f(x) = g|_{[0,1]}(x) + \Pr[X_{uv} = 1]\delta(x-1),\]
where $\delta(x)$ is Dirac delta function, by $g|_{[0,1]}(x)$ we denote:
\[
	g|_{[0,1]}(x) = \begin{cases}
	g(x) & \text{for } x \in [0,1],
	\\0 & \text{othwerwise},
	\end{cases}
\]
and:
\[
	\Pr[X_{uv} = 1] = \Pr[Y \ge 1] = \int_1^\infty g(y) \d y = 1 - \int_0^1 g(y) \d y.
\]
\begin{figure}
\centering
\begin{minipage}{0.49\textwidth}
	\includegraphics[width=\linewidth]{../mathematica/plot2_1}
\end{minipage}\hfill
\begin{minipage}{0.49\textwidth}
	\includegraphics[width=\linewidth]{../mathematica/plot2_2b}
\end{minipage}
\caption{Comparison of PDF $g(x)$ with its restricted version $g(x)|_{[0,1]}$ generated for sample parameters.}
\label{fig:plot2}
\end{figure}
Then we have:
\begin{equation*}\begin{split}
	\E[X_{uv}]	= {} & \E\left[\min\left\{ 1,\left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha \right\}\right]
				\\= {} & \int\limits_{-\infty}^{\infty} x f(x) \d x
				\\= {} & \int\limits_{0}^{1} y g(y)\d y
					+ \Pr[X_{uv} = 1]
				\\= {} & \int\limits_{0}^{1} \Pr[Y \ge y] \d y,
\end{split}\end{equation*}
with the last equality resulting from:
\[
	g(y) = \frac{\d \Pr[Y \le y]}{\d y} = -\frac{\d \Pr[Y \ge y]}{\d y},
\]
\begin{equation*}\begin{split}
	\int\limits_{0}^{1} y g(y)\d y
		= {} & \int\limits_{0}^{1} y (-\Pr[Y\ge y])'\d y
		\\= {} & \int\limits_{0}^{1} \Pr[Y \ge y]\d y - \left. \Pr[Y \ge y] y \right|_{y=0}^1
		\\= {} & \int\limits_{0}^{1} \Pr[Y \ge y]\d y - \Pr[Y \ge 1].
\end{split}\end{equation*}

Let us now count complementary cumulative distribution of $Y$:
\begin{equation*}\begin{split}
	\Pr[Y \ge y]
		= {} & \Pr \left[ \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha \ge y \right]
		\\= {} & \Pr \left[  \dabs{x_u - x_v}^{-d\alpha} \ge y \left(\frac{W}{w_u w_v}\right)^\alpha \right].
\end{split}\end{equation*}

Using the definition of our metrics (Equation \ref{eq:metric})  we get for any $a > 0$:

\begin{align*}
	\Pr\left[\dabs{x_u - x_v}^{-d\alpha} \ge a \right]
		= {} & \Pr\left[\left( \max_{i = 1,\ldots,d}\abs{x_{v_i} - x_{u_i}}_c\right)^{-d\alpha} \ge a \right]
		\\= {} & \Pr\left[ \min_{i = 1,\ldots,d}\left(\abs{x_{v_i} - x_{u_i}}_c^{-d\alpha}\right) \ge a \right]
		\\= {} & \Pr\left[\Forall_{i = 1,\ldots,d}\left(  \abs{x_{v_i} - x_{u_i}}_c^{-d\alpha} \ge a\right) \right]
		\\= {} & \prod_{i = 1}^{d} \Pr\left[ \abs{x_{v_i} - x_{u_i}}_c^{-d\alpha} \ge a \right]
		\\= {} & \prod_{i = 1}^{d} \Pr\left[ \abs{x_{v_i} - x_{u_i}}_c \le a^{-\frac{1}{d\alpha}} \right]
		\\= {} & \begin{cases}
				\left( 2a^{-\frac{1}{d\alpha}} \right)^d = 2^d a^{-\frac1\alpha}		& \quad \text{if } a \ge 2^{d\alpha}\\
				1										& \quad \text{otherwise} \\
		\end{cases}
\end{align*}
Where the last equality comes from an observation, that as $x_{u_i}$ is drawn uniformly from $[0,1]$, then $\abs{x_{v_i} - x_{u_i}}_c$ is drawn uniformly from $[0,0.5]$.
\FloatBarrier

Putting results together we obtain:
\begin{equation*}\begin{split}
	\Pr[Y \ge y]
		= {} & \Pr \left[  \dabs{x_u - x_v}^{-d\alpha} \ge y \left(\frac{W}{w_u w_v}\right)^\alpha \right]
		\\= {} & \begin{cases}
						2^d y^{-\frac1\alpha} \frac{w_u w_v}{W}		& \quad \text{if } y \ge \left( \frac{2^dw_uw_v}{W} \right) ^{\alpha}\\
						1										& \quad \text{otherwise} \\
				\end{cases}
\end{split}\end{equation*}

To simplify further calculations we denote $\left( \frac{2^dw_uw_v}{W} \right) ^{\alpha}$ by $y^*$ and obtain:
\[
	\Pr[Y \ge y]
		= \begin{cases}
				\left(\frac{y^*}{y}\right) ^{\frac1\alpha}		& \quad \text{if } y \ge y^*\\
				1										& \quad \text{otherwise} \\
			\end{cases}.
\]
Resulting cumulative probability distribution $\Pr[Y \leq y]$ is plotted on Figure \ref{fig:plot3}.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{../mathematica/plot3_b}
\caption{Cumulative probability distribution $Pr[Y \leq y] = 1 - Pr[Y \ge y]$  of Y for different values of $\alpha$.}
\label{fig:plot3}
\end{figure}

Going back to expected value of $X_{uv}$ we get two cases:
\begin{itemize}
	\item $y^* \ge 1 \iff w_u \ge \frac{W}{2^dw_v}$\\*
		In this case, as $y^* \geq y \in [0,1]$, we get $Pr[Y \ge y] = 1$ and
		\begin{equation}
			\label{eq:EXuv1}
			\E[X_{uv}]= \int\limits_{0}^{1} \Pr[Y \ge y] \d y = 1.
		\end{equation}
	\item $y^* < 1$\lineBreakWithTotallyNoPageBreak
		In this case, we divide integral into parts where $y < y^* $ and $y \ge y^*$
		\begin{equation}
			\begin{split}
			\label{eq:EXuv2}
			\E[X_{uv}]	= {} & \int\limits_{0}^{1} \Pr[Y \ge y] \d y
					\\= {} & \int\limits_{0}^{y^*} 1 \d y
							+ \int\limits_{y^*}^{1}\left(\frac{y^*}{y}\right) ^{\frac1\alpha} \d y
					\\= {} & y^* +\frac{\alpha}{\alpha - 1}\left( \left( y^* \right)^\frac 1 \alpha - y^* \right)
					\\= {} & \frac{\alpha}{\alpha - 1} \left( y^* \right)^\frac{1}{ \alpha} - \frac{1}{\alpha - 1} y^*
					\\= {} & \frac{\alpha}{\alpha - 1} \frac{2^dw_uw_v}{W} - \frac{1}{\alpha - 1} \left( \frac{2^dw_uw_v}{W} \right) ^{\alpha}
			\end{split}
		\end{equation}
\end{itemize}

From these results we can observe, that for sufficiently large $w_u w_v$ given vertices are surely connected, without any impact of their positions $x_u$, $x_v$.


\section{Expected degree of a vertex}
\label{sec:vertexDeg}

We will now estimate the expected value of a vertex degree, proving Theorem \ref{th:expectedDegree}. For any vertex $v$ with fixed weight $w_v$ and position $x_v$ we have:
\[
	\E[\deg(v)] = \E\left [\sum_{u \neq v} X_{uv}\right ] = \sum_{u \neq v} \E[X_{uv}]
	= \sum_{u = 1}^n \E[X_{uv}] - \E[X_{vv}].
\]
Let us split this sum into two parts by $u^* = \max\left\{u \in \mathbb{N}: w_u w_v \ge \frac{W}{2^d}\right\}$:
\begin{equation*}\begin{split}
	\E[\deg(v)]
		= {} & \sum_{u = 1}^{u^*} \E[X_{uv}] + \sum_{u = u^*}^n \E[X_{uv}] - \E[X_{vv}] \\
		= {} & u^* + \sum_{u = u^*}^n \E[X_{uv}] - \E[X_{vv}] \\
\end{split}\end{equation*}

To obtain $u^*$ we will first count $W$ from the definition of weights in Equation \ref{eq:weights}:
\[
	W = \sum\limits_{v = 1}^nw_v
	= \sum\limits_{v = 1}^n\delta\left(\frac{n}{v}\right)^{\frac{1}{\beta - 1}}
	= \delta n^{\frac{1}{\beta - 1}} H_{n, {\frac{1}{\beta - 1}} }
\]
Using asymptotic given in Equation \ref{eq:asymptotics2}, as $\beta \in (2,3) \Rightarrow \frac{1}{\beta - 1} \in \left (\frac{1}{2},1\right )$, we have:
\[
	H_{n,\frac{1}{\beta - 1}}
	= \frac{\beta - 1}{\beta - 2}n^{\frac{\beta - 2}{\beta - 1}} + \Theta\left( 1 \right)
\]
and:
\[
	W = \delta  \frac{\beta - 1}{\beta - 2}n + \Theta\left( n^{\frac{1}{\beta - 1}} \right) = \Theta\left( n \right).
\]
now we can return to $u^*$.
It can be calculated from its definition:
\begin{equation*}\begin{split}
	u^* = {} & \max\left\{u \in \mathbb{N}: w_u w_v \ge \frac{W}{2^d}\right\}\\
	= {} & \max\left\{u \in \mathbb{N}:
		\delta \left( \frac{n}{u} \right)^\frac{1}{\beta - 1}
		\delta \left( \frac{n}{v} \right)^\frac{1}{\beta - 1} \ge
		\frac{W}{2^d}\right\}\\
	= {} & \max\left \{u \in \mathbb{N}:
		u \le
		\frac{n^2}{v}
		\left(\frac{2^d \delta^2}{W}\right)^{\beta - 1}\right\}\\
		\in {} & \left(
		\frac{n^2}{v} \left(\frac{2^d \delta^2}{W}\right)^{\beta - 1} - 1,
		\frac{n^2}{v} \left(\frac{2^d \delta^2}{W}\right)^{\beta - 1}
		\right],
\end{split}\end{equation*}
so:
\[
	u^*
		= \frac{n^2}{v} \left(\frac{2^d \delta^2}{W}\right)^{\beta - 1} + O(1)
		= \frac{n^2}{v} \Theta\left(n^{-(\beta - 1)}\right)
		= \Theta\left(\frac{n^{3 - \beta}}{v}\right).
\]

Now we will look at the second part of the sum:

\[
	\sum_{u = u^*}^n \E[X_{uv}]
	= \sum_{u = u^*}^n \left( \frac{\alpha}{\alpha - 1} \frac{2^dw_uw_v}{W} - \frac{1}{\alpha - 1} \left( \frac{2^dw_uw_v}{W} \right) ^{\alpha}\right)
\]
Splitting it further we have:
\begin{equation*}
\begin{split}
	\sum_{u = u^*}^n \left( \frac{\alpha}{\alpha - 1} \frac{2^dw_uw_v}{W}\right)
		= {} & \frac{\alpha}{\alpha - 1} 2^dw_v \frac{W - \delta n^{\frac{1}{\beta - 1}} H_{u^*-1, \frac{1}{\beta - 1}}}{W}
		\\= {} & \frac{\alpha}{\alpha - 1} 2^dw_v
		+ \frac{
			\Theta\left(
				\frac{n^{\frac{2}{\beta - 1}}}{v^{\frac{1}{\beta - 1}}}
				\frac{n^{(3-\beta)(1-\frac{1}{\beta - 1})}}{v^{\frac{\beta - 2}{\beta - 1}}}
			\right)
		}{\Theta\left(n\right)}
		\\= {} & \frac{\alpha}{\alpha - 1} 2^dw_v + \Theta\left(\frac{n^{3 - \beta}}{v}\right)
\end{split}
\end{equation*}
and:
\[
	\sum_{u = u^*}^n \frac{1}{\alpha - 1} \left( \frac{2^dw_uw_v}{W} \right) ^{\alpha}
		=\frac{2^{d\alpha}\delta^\alpha}{\alpha - 1} \frac{w_v^\alpha n^{\frac{\alpha}{\beta - 1}} }{W^\alpha}\left(H_{n, \frac{\alpha}{\beta -1}} - H_{u^*, \frac{\alpha}{\beta -1}}\right)
\]
In this part we have additionally two possible cases:
\begin{itemize}
\item $\alpha \neq \beta - 1$

\begin{equation*}\begin{split}
	H_{n,\frac{\alpha}{\beta-1}}
		= {} & \zeta\left(\frac{\alpha}{\beta-1}\right)
		+ \Theta\left(n^{1-\frac{\alpha}{\beta - 1}}\right)
\end{split}\end{equation*}
\begin{equation*}\begin{split}
	H_{u^*,\frac{\alpha}{\beta-1}}
		= {} & \zeta\left(\frac{\alpha}{\beta-1}\right)
			+ \Theta\left(
				\frac{
					n^{(3-\beta)\left(1-\frac{\alpha}{\beta - 1}\right)}
				}{v^{1-\frac{\alpha}{\beta - 1}}}
			\right)
\end{split}\end{equation*}

\begin{equation*}\begin{split}
	\frac{w_v^\alpha n^{\frac{\alpha}{\beta - 1}} }{W^\alpha}\left(H_{n, \frac{\alpha}{\beta -1}} - H_{u^*, \frac{\alpha}{\beta -1}}\right)
	= {} & \Theta\left(\frac{n^\frac{2\alpha}{\beta - 1}}{n^\alpha v^{\frac{\alpha}{\beta - 1}}}\right)
		\Theta
		\left(
			n^{1-\frac{\alpha}{\beta - 1}}
			+ \frac{
				n^{(3-\beta)\left(1-\frac{\alpha}{\beta - 1}\right)}
			}{v^{1-\frac{\alpha}{\beta - 1}}}
		\right)
	\\= {} & \Theta\left(
		\frac{
			n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
		}{
			v^{\frac{\alpha}{\beta - 1}}
		}
		+ \frac{ n^{3 - \beta}}{v}
	\right)
\end{split}\end{equation*}


\item $\alpha = \beta - 1$

\[	H_{n}
		= \gamma
		+ \log{n}
		+ O\left(n^{-1}\right)
\]
\[
	H_{u^*}
		= \gamma
		+ \log{u^*}
		+ O\left({u^*}^{-1} \right)
		= \gamma + \Theta\left(\log{n} - \log{v}\right)
		= \gamma + \Theta\left(\log{n}\right)
\]

\begin{equation*}\begin{split}
	\frac{w_v^\alpha n^{\frac{\alpha}{\beta - 1}} }{W^\alpha}\left(H_{n, \frac{\alpha}{\beta -1}} - H_{u^*, \frac{\alpha}{\beta -1}}\right)
	= {} & \frac{\Theta\left(\frac{n}{v}\right)\Theta\left(n\right)}{\Theta(n^\alpha)}
		\left(
			\log{n}
			- \log{u^*}
			+ O\left(n^{-1}\right)
		\right)
	\\= {} & \Theta\left(\frac{n^{2 - \alpha}}{v}\right)
		\Theta\left( \log(n)\right)
	\\= {} & \Theta\left(\frac{n^{3 - \beta}}{v} \log{n}\right)
\end{split}\end{equation*}

\end{itemize}

Putting it together we get:
\[
	\sum_{u = u^*}^n \E[X_{uv}]
	= \begin{cases}
		\frac{\alpha}{\alpha - 1} 2^dw_v +
		\Theta\left(
			\frac{n^{3 - \beta}}{v}
			+ \frac{
				n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
			}{
				v^{\frac{\alpha}{\beta - 1}}
			}
		\right)
		&\text{ for } \alpha \neq \beta - 1,
		\\	\frac{\alpha}{\alpha - 1} 2^dw_v + \Theta\left(\frac{n^{3 - \beta}}{v} \log{n}\right)
		&\text{ for } \alpha = \beta - 1.
	\end{cases}
\]

Regarding $\E[X_{vv}]$, we can observe that it is bounded by $1$:
\[
	\E[X_{vv}]
	= \min\left\{1, \frac{\alpha}{\alpha - 1} \frac{2^dw_vw_v}{W} - \frac{1}{\alpha - 1} \left( \frac{2^dw_vw_v}{W} \right) ^{\alpha}\right\}
	= O(1)
\]

Going back to the whole expression:
\begin{itemize}
\item for $\alpha \neq \beta - 1$
	\begin{equation*}\begin{split}
		\E[\deg(v)] = {} &
			u^*
			+ \sum_{u = u^*}^n \E[X_{uv}]
			- \E[X_{vv}]\\
			= {} &
			\Theta\left( \frac{n^{3 - \beta}}{v}\right)
			+\frac{\alpha}{\alpha - 1} 2^dw_v
			+ \Theta\left(
				\frac{n^{3 - \beta}}{v}
				+ \frac{
					n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
				}{
					v^{\frac{\alpha}{\beta - 1}}
				}
			\right)
			+ O(1)\\
			= {} &
			\frac{\alpha}{\alpha - 1} 2^dw_v
			+ \Theta\left(
				\frac{n^{3 - \beta}}{v}
				+ \frac{
					n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
				}{
					v^{\frac{\alpha}{\beta - 1}}
				}
			\right)
	\end{split}\end{equation*}
\item for $\alpha = \beta - 1$
	\begin{equation*}\begin{split}
		\E[\deg(v)] = {} &
			u^*
			+ \sum_{u = u^*}^n \E[X_{uv}]
			- \E[X_{vv}]\\
			= {} &
			\Theta\left(\frac{n^{3 - \beta}}{v}\right)
			+\frac{\alpha}{\alpha - 1} 2^dw_v +\Theta\left(\frac{n^{3 - \beta}}{v} \log{n}\right)
			+ O(1)\\
			= {} &
			\frac{\alpha}{\alpha - 1} 2^dw_v + \Theta\left(\frac{n^{3 - \beta}}{v} \log{n}\right)
	\end{split}\end{equation*}

\end{itemize}

It is worth noting, that although $n^{3 - \beta} \log{n} = o\left(w_v\right) = o\left(n^{\frac{1}{\beta - 1}}\right)$ for $\beta \in (2,3)$, they are comparable for relatively large $n$ - for example for $\beta = 2.5$, $n^{3 - \beta} \log{n} > n^{\frac{1}{\beta - 1}} $ for $n$ from $4$ up to $24128091 \approx 2.4 \cdot 10^7$, whether for $\beta = 2.2$, it occurs for n from $3$ up to $\sim2\cdot10^{65}$.
Difference in results can be seen in the Figure \ref{fig:plotDegV}.

In general the expected value of vertex degree can be written in form:
\[\E[\deg(v)] =
	\frac{\alpha}{\alpha - 1} 2^dw_v
	+ O\left(
		\frac{n^{3 - \beta}}{v}  \log{n}
		+ \frac{
			n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
		}{
			v^{\frac{\alpha}{\beta - 1}}
		}
	\right)
\]

\begin{figure}
\centering

	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotDegV}
	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotDegVBad}

	\caption{Comparison of degrees of vertices from simulation with degrees estimated by $\E[\deg(v)] = \frac{\alpha}{\alpha - 1} 2^d\left(\frac{n}{v}\right)^{\frac{1}{\beta - 1}}$ for $n = 300, d = 3, \beta = 2.5, \delta = 1$.
	$\alpha$ is equal to $20$ the first plot and $1.5$ in the second.
	We see that estimation is good for big value of $\alpha$ even for relatively small $n$, whether for smaller values of $\alpha$ it is far worse.
	Degrees are shown in logarithmic scale for better visibility.}
	\label{fig:plotDegV}
\end{figure}


\section{Expected number of edges}

To prove Theorem \ref{th:expectedEdges} we will compute expected number of edges $\E[m]$ using obtained expected vertices degrees:
\begin{itemize}
\item for $\alpha \neq \beta - 1$

\begin{align*}
	\E[m]
		&= \frac{\sum_{v = 1}^n \E[\deg[v]]}{2}
		\\&= \frac{1}{2} \sum_{v = 1}^n\left(
			\frac{\alpha}{\alpha - 1} 2^dw_v
			+ \Theta\left(
				\frac{n^{3 - \beta}}{v}
				+ \frac{
					n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
				}{
					v^{\frac{\alpha}{\beta - 1}}
				}
			\right)
		\right)
		\\&= \frac{2^{d-1}\alpha}{\alpha - 1}  \sum_{v = 1}^n w_v
		+ \Theta\left(n^{3 - \beta}\right)  \sum_{v = 1}^n\frac{1}{v}
		+ \Theta\left(n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }\right)  \sum_{v = 1}^n\frac{1}{v^{\frac{\alpha}{\beta - 1}}}
		\\&= \frac{2^{d-1}\alpha}{\alpha - 1} W
		+ \Theta\left(n^{3 - \beta}\right)  H_n
		+ \Theta\left(n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }\right) H_{n, \frac{\alpha}{\beta - 1}}
		\\&= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
		+ \Theta\left(n^{3 - \beta}\log{n}\right)
		+ \Theta\left(n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }\left(1 + n^{1-\frac{\alpha}{\beta - 1}} \right)\right)
		\\&= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
		+ \Theta\left(
			n^{3 - \beta}\log{n}
			+ n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} }
			+ n^{2 - \alpha}
		\right)
\end{align*}
Because $n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} } = o\left(n^{2 - \alpha}\right)$ for $ \alpha < \beta - 1$ and
$n^{ 1 - \alpha\frac{\beta - 2}{\beta-1} } = o\left(n^{3-\beta}\right)$ for $ \alpha > \beta - 1 $, we get:
\[
	\E[m]
	= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
		+ \Theta\left(
			n^{3 - \beta}\log{n}
			+ n^{2 - \alpha}
		\right)
\]

\item for $\alpha = \beta - 1$
\begin{equation*}\begin{split}
	\E[m]
		&= \frac{\sum_{v = 1}^n \E[\deg[v]]}{2}
		\\&= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
			+ \Theta\left(n^{3 - \beta} \log{n}\right)\sum_{v = 1}^n\frac{1}{v}
		\\&= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
			+ \Theta\left(n^{3 - \beta} \log^2{n}\right)
\end{split}\end{equation*}

And for any $\alpha > 1$ we obtain:
\begin{equation*}\begin{split}
	\E[m]&= \delta 2^{d-1} \frac{\alpha}{\alpha - 1} \frac{\beta - 1}{\beta - 2} n
			+ O\left(
				n^{3 - \beta} \log^2{n}
				+ n^{2 - \alpha}
			\right)
\end{split}\end{equation*}

\end{itemize}

\section{Simulations}
\FloatBarrier
In the following figures we can see results of simulations compared with the estimation given above.
We can see, that the linear relation between number of generated edges $m$ and number of nodes $n$ is very well observed in real simulations.
We can however see, that relation between slope of this relation and model parameters is not perfect.
Especially for small value of $\beta$ or $\alpha$ the number of edges is overestimated.
It results straight from the cause given at the end of Section \ref{sec:vertexDeg} -- for such parameters, even for very large $n$ we have $n^{2-\alpha}$ and especially $n^{3-\beta}\log{n}$ close to $n$ (or even bigger).
As $d$ and $\delta$ do not appear in $n$'s exponent, their impact on slope is well represented by the estimation.

Looking at the figures we can also see strange steps in the simulation plots.
It shows, that the optimized algorithm, shown in Section \ref{sec:optimizedAlgorithm}, is not perfect and gives only estimation of the graphs given by the GIRG model.
Such steps should not be present in simulation by the basic algorithm (Section \ref{sec:basicAlgorithm}), but its time complexity yields its usage infeasible.

\begin{figure}
\centering

	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotD}

	\caption{Comparison of generated and estimated number of edges for $\alpha = 2$, $\beta = 2.5$, $\delta = 1$ and variable $d$.}
	\label{fig:plotDegD}
\end{figure}


\begin{figure}
\centering

	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotAlpha}

	\caption{Comparison of generated and estimated number of edges for $d = 3$, $\beta = 2.5$, $\delta = 1$ and variable $\alpha$.}
	\label{fig:plotDegAlpha}
\end{figure}


\begin{figure}
\centering

	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotBeta}

	\caption{Comparison of generated and estimated number of edges for $d = 3$, $\alpha  = 1.5$, $\delta = 1$ and variable $\beta$.}
	\label{fig:plotDegBeta}
\end{figure}


\begin{figure}
\centering

	\includegraphics[width=\linewidth]{../mathematica/expectedEdgesPlotDelta}

	\caption{Comparison of generated and estimated number of edges for $d = 3$, $\alpha = 2$, $\beta = 2.5$ and variable $\delta$.}
	\label{fig:plotDegDelta}
\end{figure}

