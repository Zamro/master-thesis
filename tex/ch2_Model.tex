\chapter{Geometric Inhomogeneous Random Graphs model}

The Geometric Inhomogeneous Random Graphs (GIRG) is a relatively new model of random graphs, introduced in 2015 by Bringmann, Keusch and Lengler\cite{GIRG}.
It is an extension of a widely known Chung-Lu model\cite{chung2002average}, in which probability of connecting two nodes is based on their weights.
Thanks to this, the model has the ability to set expected degrees of vertices by adjusting these weights.
GIRG model combine this property together with being position aware, so connection between vertices is more probable if they lay closer to each other.

In the original paper authors prove that graphs sampled from this model follows many properties of complex networks, i.e. small-world and clustering, making it a good model for analysing behaviour of algorithms designed to work on real-life graphs.
They also states, that the main advantage over other models which join scale-freeness with underlying geometry is its simplicity combined with independence of edges.

In this section we will define this model, show how the weights $w_v$ and positions $x_v$ are assigned for each of the vertices $v \in \{1,\ldots,n\}$, and how they are used during graph generation.
It must be noted, that presented model is more concrete than the original one in the following aspects:
\begin{itemize}
	\item weights are generated in a strict way, given by Equation \ref{eq:weights},
	\item probability of connecting two vertices is precisely defined, instead of being just asymptotically bounded.
\end{itemize}

\section{Notation}
\label{sec:notation}
\label{sec:parameters}
We will denote the GIRG model by $\mathcal G = \mathcal G (n, d, \alpha, \beta, \delta)$, and graph sampled from it by $G = (V, E)$, where $V = \oneTo{n}$ is a set of vertices, and $E \subset \{ \{v, u\} : v,u \in V \land v\neq u \}$ is a set of generated undirected edges.
Additionally we will denote number of generated edges by $m = \abs{E}$.
As we can see the model has 5 parameters, which will be better explained in further sections:
\begin{itemize}
	\item $n \in \mathbb{N}$ -- number of nodes in generated graph,
	\item $d \in \mathbb{N}$ -- dimension of torus, from which positions are sampled,
	\item $\alpha > 1$ -- power to which probability of connecting two vertices is raised,
	\item $\beta \in (2,3)$ -- exponent of the power law, from which weights are chosen,
	\item $\delta > 0$ -- scale of weights, minimal weight.
\end{itemize}

During generation of graph, as previously noted, weights  $w_v$ and positions  $x_v$ of each vertex $v \in V$ are generated.
We will also use  $w_{min} = \min\{w_i: i \in V\}$, $w_{max} = \max\{w_i: i \in V\}$ and $W = \sum_{v \in V} w_v$.
Probability of connecting two vertices $u$ and $v$ is denoted by $p_{uv} = \Pr[u \sim v]$.

\FloatBarrier
\section{Weights}
We require that weights of vertices satisfy the power law with exponent $\beta \in (2, 3)$.
It means, that the number of vertices with weights $k$ must be proportional to $k^{-\beta}$.
In general it can be achieved by sampling weights $w_v$ from a power-law probability distribution given by probability density function:
\[
	f(w) = \frac{\beta - 1}{\delta}\left(\dfrac{w}{\delta}\right)^{-\beta},
\]
where $\delta > 0$ is the lowest possible weight, from which the law holds.
However, in this paper we assume, that the weights are deterministically generated in the following way:
\begin{equation}
	\label{eq:weights}
	w_v = \delta \left(\frac{n}{v} \right)^\frac{1}{\beta-1}.
\end{equation}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{../mathematica/plot1}
\includegraphics[width=\linewidth]{../mathematica/plot1_legend}
\caption{Comparison of deterministically generated weights and power-law distribution.}
\label{fig:plot1}
\end{figure}
In this way we obtain deterministic values, which obey the power law (see Figure \ref{fig:plot1}).

\section{Position and distance}
Although Bringmann et al. state in their works\cite{GIRG}  \cite{generalGIRG}, that many of their results stays correct also for any metric space, from which we can uniformly sample positions, they define GIRG as more concrete model, in which positions are sampled uniformly from $d$-dimensional torus with maximum norm $\dabs{\cdot}$ used as metric:
\begin{equation}
	\label{eq:metric}
	\dabs{x - y} = \max_{i = 1,\ldots,d}\abs{x_i - y_i}_c,
\end{equation}
where $\abs{x - y}_c$ is a distance on a unit circle:
\[
	\abs{x_i - y_i}_c = \min\{\abs{x_i - y_i}, 1 - \abs{x_i - y_i}\}.
\]
We will also follow this assumption.


\section{Edge probability}
Given two vertices $v$ and $u$ with fixed weights  $w_v$, $w_u$ and positions $x_v$, $x_u$, probability of connecting them is given by:
\[
	p_{v,u} = \Theta\left(\min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}\right),
\]
where $\alpha > 1$.

Although $\Theta$ allows GIRG to cover wider range of existing models, for concreteness in this paper we assume simpler form:
\begin{equation}
	\label{eq:prob}
	p_{v,u} = \min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}.
\end{equation}

