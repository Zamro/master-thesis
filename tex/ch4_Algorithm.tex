\SetNoFillComment
\SetKwProg{Fn}{Function}{}{}
\SetKwComment{Comment}{(}{)}
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\SetKwFunction{ordering}{createOrdering}
\SetKwFunction{ptI}{convertPositionIntoIndex}
\SetKwFunction{divideVerticesIntoLayers}{divideVerticesIntoLayers}
\SetKwFunction{divideLayersIntoCells}{divideLayersIntoCells}
\SetKwFunction{sampleVertices}{sampleVertices}
\SetKwFunction{getNeighbours}{getNeighbours}
\SetKwFunction{getTypeIPairs}{getTypeIPairs}
\SetKwFunction{getTypeIIPairs}{getTypeIIPairs}

\newcounter{mycount}

\newcommand{\gridOnly}[2]{
	\def\l{#1}
	\def\size{#2}
	\pgfmathsetmacro\step{(\size/2.^\l}
	\draw[step=\step,color=gray, thick, fill=white] (0,0) grid (\size,\size)  rectangle (0,0);
}

\newcommand{\indexPicture}[2]{
	\begin{tikzpicture}
	\draw[step=3,color=darkgray, very thick] (0,0) grid (6, 6);

	\def\l{#1}
	\def\size{#2}
	\pgfmathsetmacro\step{(\size/2.^\l}
	\pgfmathtruncatemacro\numInRow{2^\l}
	\draw[step=\step,color=gray, thick, fill=white] (0,0) grid (\size,\size)  rectangle (0,0);

	\setcounter{mycount}{0}
	\foreach \y in {1,...,\numInRow}
	{
		\foreach \x in {1,...,\numInRow}
		{
			\pgfmathsetmacro\X{\step*(\x - 0.5)}
			\pgfmathsetmacro\Y{\size - \step*(\y - 0.5)}
			\node at (\X, \Y) {\arabic{mycount}\addtocounter{mycount}{1}};
		}
	}

	\end{tikzpicture}
}

\chapter{Sampling algorithm}

\section{Basic algorithm}
\label{sec:basicAlgorithm}

The simplest way to sample GIRG is to decide separately for each possible edge if it should be included in resulting graph, using  probability given by Equation \ref{eq:prob}.
It is done after initializing vertices with weights (generated with deterministic method, as in Equation \ref{eq:weights}) and positions (sampled independently uniformly from $(0,1)^d$), as shown in the function \sampleVertices{}.
Complexity of such algorithm is $O(n^2)$, as one must iterate over each of $\binom{n}{2}$ possible edges.
It is shown in Algorithm \ref{alg:simpleAlgorithm}.

\begin{function}
	\small
	\caption{sampleVertices($n$, $\beta$, $w$)}
	\Input{$n$, $d$, $\beta$, $\delta$ -- as defined in Section \ref{sec:parameters}
	}
	\Output{
		$x$ -- sampled positions\newline
		$w$ -- sampled weights
	}
	\For{$i \leftarrow 1 \ldots n$}{
		$w_i = \delta\left(\frac{n}{i}\right)^\frac{1}{\beta-1}$\;
		\For{$j \leftarrow 1 \ldots d$}{
			Sample $x_i[j]$ uniformly from $[0,1)$ \;
		}
	}
	\KwRet $x$, $w$ \;
\end{function}

\begin{algorithm}
	\small
	\caption{simpleAlgorithm}
	\label{alg:simpleAlgorithm}
	\Input{$n$, $d$, $\alpha$, $\beta$, $\delta$ -- as defined in Section \ref{sec:parameters}
	}
	\Output{
		$E$ -- set of edges in generated graph
	}
	$x$, $w$ $\leftarrow$ \sampleVertices{$n$, $\beta$, $w$} \;
	$W = \sum_{i = 1}^n w_i$ \;
	$E \leftarrow \emptyset$ \;
	\For{$u \leftarrow 1 \ldots n$}{
		\For{$v \leftarrow u+1 \ldots n$}{
			Sample $b$ from Bernoulli$\left(\min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}\right)$\;
			\If{$ b = 1 $}{
				$E = E \cup \{(u, v)\}$
			}
		}
	}
	\KwRet $E$ \;
\end{algorithm}

\section{Optimized algorithm}
\label{sec:optimizedAlgorithm}

Authors of GIRG model in their work\cite{GIRG} presented an $O(n)$ algorithm,
which is using geometric distribution to simulate multiple failures and accelerate edges sampling.
It is achieved by creating structure, which divides vertices into regions called cells, both by their weights and positions.
This structure makes it possible to select pairs of cells which partition the space and between which probabilities of connecting any two vertices are bounded above by some computed values.
This value is further used in choosing possible edges from geometric distribution, and for each candidate its outcome is made precise by sampling from Bernoulli trial with properly adjusted probability.
Authors further prove, that algorithm works in expected time $O(m)$, which equals to $O(n)$ thanks to $\E[m] = O(n)$ (see Theorem \ref{th:expectedEdges}).

In this chapter we will show this algorithm in more detail. Whereas original paper provides great insight into reasoning behind the algorithm and extensive analysis of its performance, pseudocode of the algorithm is written on very high level of generality and relatively big effort is required to transform it into a working program.
We will also prove, that it fulfils complexity requirements.


\subsection{Data structure}
Fundamental to this algorithm is its data structure, created in the preprocessing stage and used in the actual algorithm.

\subsubsection{Layers}
Vertices are first divided into layers by their weight:
\begin{definition}[Layer]
	For $i \geq 0$ we define $i$-th layer as:
	\[
		L_i  = \left\{v\in \oneTo n : w_{min}2^i \le w_v < w_{min}2^{i+1} \right\}.
	\]
\end{definition}
\begin{sloppypar}
As weights are bounded by $w_{max}$, all layers for $i > \floor{\log_2\left(\frac{w_{max}}{w_{min}}\right)}$ will be empty and not used.
We will denote a set of all non-empty layers by $ \mathcal{L}$.
Implementation of this split is straightforward and shown in the function \divideVerticesIntoLayers{}.
\end{sloppypar}
\begin{function}
	\small
	\caption{divideVerticesIntoLayers($n$, $w$)}
	\Input{$n$ -- number of vertices\newline
		$w$ -- array of vertices weights
	}
	\Output{$L$ -- array of layers, each being an array of vertices
	}
	\For{$i \leftarrow 0 \ldots \floor{\log_2\left(\frac{w_{max}}{w_{min}}\right)}$}{
		$L_i = \emptyset$\;
	}
	\For{$v \leftarrow 1 \ldots n$}{
		$j = \floor{ \log_2{\left(\frac{w_v}{w_{min}}\right)} }$\;
		$L_j = L_j \cup \{v\}$\;
	}
	\KwRet L;
\end{function}

\subsubsection{Cells}
Layers are further divided into cells by their positions:
\begin{definition}[Cells]
	For $l \in \mathbb{N}$ and $\vec{i} \in \mathbb{N}^d, \forall_{j \in \oneTo d } i_j < 2^{l}$ we define cell as:
	\[
		C(l, \vec{i})
			= \left[i_1 2^{-l}, (i_1 + 1)2^{-l}\right )
				\times \ldots \times
			\left [i_d 2^{-l}, (i_d + 1)2^{-l}\right ).
	\]
	Additionally we can see, that $\vec i$ can be replaced by $\vec x \in [0,1)^d$ - position of cell's corner closest to $\vec{0}$ in euclidean metric:
	\[
		C(l, \vec{i})
		= C(l, \vec{x})
		= \left [x_1, x_1 + 2^{-l}\right )\times \ldots \times \left [x_d, x_d + 2^{-l}\right ),
	\]
	where $\forall_{j \in \oneTo d } x_j = 2^l i_j $, or by $I \in \mathbb{N}$ of the form:
	\[I = \sum\limits_{j = 1}^{d}i_j 2^{l(j-1)}.\]
	Because $i_j < 2^l$, this conversion is a bijection between set of possible $\vec{i}$ and $\oneTo{2^{ld}-1}$.
	We will call $l$ a level of a cell, $I$ its index and $\vec x$ its position.
	We will denote conversions between a position $\vec{x}$ and index $I$ by $I(\vec{x})$ and $\vec{x}(I)$, and also use $\vec x(C)$ for a position of a cell $C$ and $I(C)$ for its index.

	Additionally we will denote partition of $[0,1)^d$ into cells on level $l$ by:
	\[
		\mathcal{C}_l = \left\{C(l, I) : I \in \zeroTo{2^{ld} -1} \right\}.
	\]
\end{definition}

We want to be able to efficiently construct a structure $\mathcal{D}_l(S)$, which, given set of vertices $S$ and maximum cell level $l$ provides a way to obtain $F_i(I) = \{v \in S : x_v \in C(i,I)\}$ for $i \in \zeroTo{l}$ and $I \in \zeroTo{2^{id}-1}$.
Additionally it must have (any) strict order $X(i,I)$ for each $F_i(I)$.
It have to be constructible in $O(\abs{S} + 2^{ld})$ and allow to obtain $\abs{F_i(I)}$ and $k$-th vertex from $F_i(I)$ (with respect to $X(i,I)$) in constant time.

To obtain such structure, we will need another ordering of cells -- geometric ordering.
Firstly we create a tree $T_l$ of cells up to level $l$ such that:
\begin{itemize}
	\item $C(0,0)$ is a root of $T_l$,
	\item for each cell $C \in \mathcal{C}_i$ for $i \in \oneTo{l}$ we set its parent to $P$ such that $P \in \mathcal{C}_{i-1}$ and $\vec x(C) \in P$.
\end{itemize}
By subcells of $P$ we will denote $\{C : P \text{ is a parent of } C \text{ in }T_l\}$.
Using this we can define:
\begin{definition}[Geometric ordering]
	We say, that cells $\mathcal{C}_l$ at level $l$ are geometrically ordered, when they are ordered according to their order during depth first search of tree $T_l$, given that for any cell their subcells are traversed in order consistent with first level indexes.
\end{definition}
In pseudocodes we will assume, that subcells are always ordered with respect to their indexes.
Comparison of both orderings is shown in the Figure \ref{fig:orderings}.

Geometric ordering has one important property: subcells of each cell are consecutively enumerated.
To obtain $\mathcal{D}_l(S)$, we proceed as follows:
given a sequence of vertices $S$ we put them in an array, ordered with respect to geometric ordering of $\mathcal{C}_l$.
As was noted before, if two or more vertices are in any single cell, their order is insignificant (although it must be fixed).
Now for cells on each level we only need to know the beginning and ending position in this array.
As vertices are in continuous memory fragment, for any chosen cell we can give both the number of vertices belonging to it, and $k$-th vertex from it in constant time.
An example memory alignment is given in the Figure \ref{fig:cellsInMemory}.

\begin{figure}
	\centering
	\begin{tabular}{m{1 cm} m{6 cm} m{6 cm}}
		level&\makebox[6cm]{Indexes}&\makebox[6cm]{Geometric ordering}
		\\
		1
		&\indexPicture{1}{6}
		&\indexPicture{1}{6}
		\\
		2
		&\indexPicture{2}{6}
		&\begin{tikzpicture}
		\draw[step=1.5,color=gray, thick] (0,0) grid (6, 6);
		\draw[step=3,color=darkgray, very thick] (0,0) grid (6, 6);
		\clip (0, 0) rectangle (6, 6);
		\matrix at (3,3)[matrix of nodes,nodes={inner sep=0pt,text width=1.5cm,align=center,minimum height=1.5cm}]{
			0 & 1 & 4 & 5 \\
			2 & 3 & 6 & 7 \\
			8 & 9 & 12 & 13 \\
			10 & 11 & 14 & 15\\};
		\end{tikzpicture}
	\end{tabular}
	\caption{Comparison of cells' orderings for $d = 2$ and levels from $1$ to $2$.}
	\label{fig:orderings}
\end{figure}

\tableAsFigure{cellsInMemory}{
	Example memory alignment of cells for $n = 23$, $d = 2$ and $l_{max} = 2$.
	Cells are numbered by their geometric order.
}

In the algorithm cells structure $\mathcal{D}$ is created for each layer $L_i$ separately.
Maximum level $l_i$ to which cells are created is set to $l_i = \left\lfloor \log_{2^d} \left( \frac{W}{2^{i+1} w_{min}^2} \right) \right\rfloor$.
It is chosen in a way, that minimal length of cells' edges is inversely proportional to weights included in layer~$L_i$.

To align vertices in memory in geometric order, we need to iterate over $L_i$ twice.
In the original approach it is said, that during the first pass only the count of vertices for each cell  is computed, so in the second pass they can be inserted into right position in the memory.
In the following pseudocode all vertices are instead stored in temporary structure, from which are later retrieved.
It has asymptotically equivalent complexity and is changed just for author convenience.
Implementation of splitting all layers into cells is shown in the function \divideLayersIntoCells, and illustration of resulting structure is shown in the Figure \ref{fig:trees}.

\input{cells3d}

\subsection{Preprocessing}
\begin{sloppypar}
During the first step, the weights and positions of vertices are established (\sampleVertices).
Furthermore, vertices are divided into several layers, differentiated by their weight (\divideVerticesIntoLayers ), and the space is divided into cells (\divideLayersIntoCells).
It creates structure described in previous paragraphs.
\end{sloppypar}

\begin{function}
	\small
	\caption{divideLayersIntoCells($d$, $L$, $x$)}
	\Input{$n$ -- number of vertices\newline
		$L$ -- array of layers, each being an array of vertices
	}
	\Output{$\vec D$ -- vector of cells' structure for each layer, such that $D_i = D[i] = \mathcal D_{l_i}(L_i)$ in a form $D_i = (\text{tab, cells})$, where tab is internal storage for vertices and $\text{cells}[j][I] = (\text{begin, end})$ such that $F_j(I) = (\text{tab}_\text{begin}, \ldots, \text{tab}_{\text{end}-1})$.
	}
	\For{$i \leftarrow 0 \ldots \abs{L} - 1$}{
		$l_i = \left\lfloor\log_{2^d}\left( \frac{W}{2^{i+1} w_{min}^2} \right)\right\rfloor$\;
		initialize temporary array $T$ with $2^{dl}$ empty lists\;\label{alg:divideLayersIntoCells:tempInitialisation}
		\For{$v \in L_i$}{
			add $v$ to $T[I(x_{v}, l_i)]$ \;\label{alg:divideLayersIntoCells:tempLocation}
		}
		idx = 1\;
		\For{$C \in \mathcal{C}_{l_i}$ in geometric order}{\label{alg:divideLayersIntoCells:it1Start}
			begin = idx\;
			\For{$v \in T[I(C)]$}
			{
				D.tab[idx] = v\;\label{alg:divideLayersIntoCells:relocation}
				idx++;
			}
			end = idx\;
			D[i].cells[$l_i$][$I(C)$] = $(\text{begin, end})$\;
		}\label{alg:divideLayersIntoCells:it1End}
		\For{$j \leftarrow l_i-1 \ldots 0$}{\label{alg:divideLayersIntoCells:it2Start}
			\For{$C \in \mathcal{C}_j$}{
				begin = subcells($C$)[0].begin\;
				end = subcells($C$)[$2^{d}-1$].end\;
				D[i].cells[$j$][$I(C)$] =$(\text{begin, end})$\;
			}
		}\label{alg:divideLayersIntoCells:it2End}
	}
	\KwRet D\;
\end{function}


\subsection{Algorithm}

While generating graph, we want to consider each possible edge once.
Looking at layers it can be achieved by taking pairs of them with different indexes $\{(u,v) \in L_i \times L_j : 1 \leq i < j \leq \abs{L}\}$, and additionally pairs with same indexes, but paying attention to not repeat the same edge or create a loop $\{(u,v) \in L_i \times L_i : 1 \leq i \leq \abs{L} \land u < v\}$.

We have defined two ways to partition a space of possible vertices -- layers enables us to divide them by their weights, whether cells and structure $\mathcal{D}$ by their position.
We now want to obtain a partitioning of $\{(u,v) \in V\times V : u < v \}$ , which enables us to bound probability of pairs in each obtained subset.
Note, that we can easily obtain $\{(u,v) \in V\times V : u < v \}$ from $V\times V$ by taking $\mathcal{P} =  \{L_i \times L_j : i<j\} \cup \{ \{(u,v) \in L_i\times L_i : u<v\} : L_i \in \mathcal{L}\}$ as partition.
We now want to partition each of $P\in\mathcal{P}$.

As described in \cite{GIRG}, we construct a partition $\mathcal{P}_l$ of $K = ([0,1)^d)^2$ such that $\mathcal{P}_l = \mathcal{P}_l^I \cup \mathcal{P}_l^{II}$  and:
\begin{itemize}
	\item $\mathcal{P}_l^I = \{(A,B) \in \mathcal{C}_l \times \mathcal{C}_l : d(A,B) = 0\}$,
	\item $\mathcal{P}_l^{II} = \bigcup_{i = 0} ^{l-1} \{(A,B) \in \mathcal{C}_i \times \mathcal{C}_i : d(A,B) > 0 \land d(\text{parent}(A),\text{parent}(B)) = 0\}$,
\end{itemize}
where $d(A,B) = \sup_{x \in A, y \in B}\dabs{x - y} $.
We call elements of $\mathcal{P}_l^I$ and $\mathcal{P}_l^{II}$ type I and type II pairs of cells respectively.
Note, that for cells on level $l$ distance $d(A,B)$ will always be a multiplication of $2^{-l}$, so $d(A,B) >0 \iff d(A,B) \geq 2^{-l}$.

To prove, that $\mathcal{P}_l$ partitions $K$ let us consider any $(x,y) \in K$.
Let us denote by $C_z^k$ cell on level $k$ which contains $z$.
If $d(C_x^l, C_y^l) = 0$, the pair $(C_x^l, C_y^l)$ is added as a pair of type I, and no further pair containing them is added.
Otherwise, as $d(C_a^k, C_b^k) < d(C_a^{k+1}, C_b^{k+1})$ for any $a,b,k$ (it follows from $C_a^{k+1}\subset C_a^k $) and  $C_x^0 = C_y^0 \rightarrow d(C_x^0, C_y^0) = 0$, there exists $l^* < l$ such that $d(C_x^{l^*}, C_y^{l^*}) = 0 \land d(C_x^{l^*+1}, C_y^{l^*+1}) > 0$, so $(C_x^{l^*}, C_y^{l^*})$ is added as type II pair, and no further pair containing them is added.

To benefit from creating such partition it is created for every pair of layers $(L_i, L_j)$ such that $i \leq j$ and maximum level of cells is set to: $l_{ij} = \left\lfloor\log_{2^d}\left( \frac{W}{2^{i+j + 2} w_{min}^2} \right)\right\rfloor$.
As $l_{ij} \leq l_i \land l_{ij} \leq l_j$, we can use $\mathcal{D}_i$ and $\mathcal{D}_j$ to create pairs.
We will denote structure $\mathcal{P}_{l_{ij}}$, together with mapping from cells to vertices from $\mathcal{D}_i$ and $\mathcal{D}_j$ by $\mathcal{D}_{ij}$
Such complicated partitioning of $K$ is done for efficiency reason -- vertices' pairs from $\mathcal{P}^I$ have high probability of connecting, whether for pairs of type II this probability can be effectively bounded from above by a value computed on cells' level and sampling edges can be accelerated, which will be explained in more details later on.

To effectively generate $\mathcal{D}_{ij}$ we need a method which, given some cell $A$ on level $l$ is able to generate set of cells neighbours $N(A) = \{ B \in \mathcal{C}_l : d(A,B) = 0 \}$.
Its implementation is somehow complicated due to multidimensionality of space and is shown in the function \getNeighbours.
This enables us to list pairs of cells belonging to both required types.
They are implemented in respectively \getTypeIPairs and \getTypeIIPairs functions.

\begin{function}
	\small
	\caption{getNeighbours($Id$, $\mathcal{D}$, $l$)}
	\Input{$Id$ -- index of cell for which we want to find neighbours\newline
		$\mathcal{D}$ -- structure with cells from which we choose neighbours\newline
		$l$ -- level on which is given cell
	}
	\Output{$N$ -- set of neighbouring cells
	}
	$N = \emptyset$\;
	\uIf{$l \leq 1$}{
		$N = \mathcal{D}[l]$\;
	}
	\Else{
		$\text{Indexes} = \{Id\}$\;
		\For{$i \leftarrow 1 \ldots d$}{
			$\text{size} = \abs{\text{Indexes}}$\;
			\For{$s \leftarrow 1 \ldots \text{size}$}{
				dist $= 2^{l(i-1)}$\tcc*[r]{\scriptsize distance in indexes when moving by one in dimension $i$}
				pos $= \frac{\text{Indexes}[s]}{\text{dist}} \mod 2^l$\tcc*[r]{$\text{Indexes}[s]_i$}
				$\text{Indexes}[\text{size} + 2s - 1] = \text{Indexes}[s] + \text{dist}((\text{pos} - 1)\mod2^l - \text{pos})$\;
				$\text{Indexes}[\text{size} + 2s] = \text{Indexes}[s] + \text{dist}((\text{pos} + 1)\mod2^l - \text{pos})$\;
			}
		}
		$N = \{\mathcal{D}[l][i] : i \in \text{Indexes}\}$\;
	}
	\KwRet $N$\;
\end{function}

\begin{function}
	\small
	\caption{getTypeIPairs($\mathcal{D}_1$, $\mathcal{D}_2$, $l$)}
	\Input{$\mathcal{D}_1$ -- cells from first layer\newline
		$\mathcal{D}_2$ -- cells from second layer\newline
		$l$ -- maximum level of partitioning
	}
	\Output{$\mathcal{D}_{ij}^{I}$ -- set of type I cells pairs
	}
	$\mathcal{D}_{ij}^{I} = \emptyset$\;
	\For{$A \in D_1[l]$}{
		\For{$B \in \getNeighbours(A.Id, D_2, l,)$}{
			$\mathcal{D}_{ij}^{I} = \mathcal{D}_{ij}^{I} \cup (A,B)$
		}
	}
	\KwRet $\mathcal{D}_{ij}^{I}$\;
\end{function}

\begin{function}
	\small
	\caption{getTypeIIPairs($\mathcal{D}_1$, $\mathcal{D}_2$, $l$)}
	\Input{$\mathcal{D}_1$ -- cells from first layer\newline
		$\mathcal{D}_2$ -- cells from second layer\newline
		$l$ -- maximum level of partitioning
	}
	\Output{$\mathcal{D}_{ij}^{II}$ -- set of type I cells pairs
	}
	$\mathcal{D}_{ij}^{II} = \emptyset$\;
	\For{$i \leftarrow 0 \ldots l - 1$}
	{
		\For{$A_{parent} \in D_1[i]$}{
			\For{$B_{parent} \in \getNeighbours(A.Id, D_2, i)$}{
				\For{$A \in subcells(A_{parent})$}{
					\For{$B \in subcells(B_{parent})$}{
						\If{$d(A,B) > 0$}
						{
							$\mathcal{D}_{ij}^{II} = \mathcal{D}_{ij}^{II} \cup (A,B)$
						}
					}
				}
			}
		}
	}
	\KwRet $\mathcal{D}_{ij}^{II}$\;
\end{function}

Using these functions we can finally write optimized algorithm, analogous to the original pseudocode\cite[Algorithm~1]{GIRG}, shown in Algorithm \ref{alg:optimizedAlgorithm}.
The algorithm, after vertices are sampled and structures are generated, is fairly plain:
\begin{itemize}
	\item for all vertices' pairs $(u,v)$ of type I, we use simple algorithm,
	\item for each cells' pair $(A,B)$ of type II, we calculate $\overline p = \min\left\{ 1, \left( \frac{ w_{min}^2 2^{i + j  + 2} }{ W } \frac{ 1 }{ d(A,B)^d } \right)^\alpha \right\}$.
	As $\forall_{(u,v) \in A\times B} (p_{uv} < \overline p)$, we can speed up sampling by approximating it with $geo(\overline p)$.
	Instead of iterating over all possible edges, we sample index of next edge candidate from geometric distribution.
	As we sample indexes repeatedly from $geo(\overline{p})$ (each time adding new value to previous index), each possible index has probability $\overline p$ of being chosen.
	If index $k$ is selected, we tweak its probability, by adding edge represented by it with probability $\frac{p_k}{\overline p}$.
	In result, for each $i \in \oneTo{\abs A \abs B}$, edge at index $i$ is chosen with probability $\overline{p}\frac{p_i}{\overline p} = p_i$.
\end{itemize}

\begin{sloppypar}
While indexes are generated with usage of geometric distribution defined by $\Pr\left[geo(p) = i\right] = \left(1-p\right)^{i-1} p$, a special attention must be given for it to run in constant time.
It can be done by sampling $r$ uniformly from $(0,1)$ and then returning a value $\left\lceil \frac{\log(R)}{\log(1-p)}\right\rceil$.
For small value of $p$ it can easily exceed data type range -- in such case it is safe to use maximum allowed value, as it will anyway be bigger than $\abs A \abs B$.
On the contrary during writing of this paper it was found out, that GCC 5.4 implementation of std::geometric\_distribution in such case loops until small enough number is generated, which resulted in whole GIRG generator running in exponential, instead of linear time.
Such behaviour is not only slowing down the computation, but also distorts the distribution -- although ratio of all possible values is preserved, the probability of values greater than the limit is shared between these lower values.
\end{sloppypar}

\begin{algorithm}
	\small
	\caption{optimizedAlgorithm}
	\label{alg:optimizedAlgorithm}
	\Input{$n$, $d$, $\alpha$, $\beta$, $\delta$ -- as defined in Section \ref{sec:parameters}
	}
	\Output{
		$E$ -- set of edges in generated graph
	}
	$x$, $w$ $\leftarrow$ \sampleVertices{$n$, $\beta$, $w$} \; \label{alg:optimizedAlgorithm:preprocStart}
	$w_{min} = \min\limits_{i = 1, \ldots, n} w_i$ \tcc*[r]{ $ = \delta$}
	$w_{max} = \max\limits_{i = 1, \ldots, n} w_i$ \tcc*[r]{ $ = \delta n^\frac{1}{\beta - 1}$}
	$W = \sum_{i = 1}^n w_i$ \;
	$E = \emptyset$ \;
	$L = $ \divideVerticesIntoLayers{$n$, $w$}\;
	$D = $ \divideLayersIntoCells{$L$, $d$}\;\label{alg:optimizedAlgorithm:preprocEnd}

	\For{$i \leftarrow 0 \ldots \abs{D} - 1$}{
		\label{alg:optimizedAlgorithm:algorithmIterStart}
		\For{$j \leftarrow i \ldots \abs{D} - 1$}{
			$l_{ij} = \left\lfloor\log_{2^d}\left( \frac{W}{2^{i + j + 2} w_{min}^2} \right)\right\rfloor$\;
			\For{$(A,B) \in \text{getTypeIPairs}(D[i], D[j], l)$}{\label{alg:optimizedAlgorithm:algorithmIStart}
				\For{$(u,v) \in A \times B$}{
					\If{$i \neq j \lor u < v$}{
						add $(u, v)$ to $E$ with probability $p_{uv} = \min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}$\;
					}
				}
			}\label{alg:optimizedAlgorithm:algorithmIEnd}
			\For{$(A,B) \in \text{getTypeIIPairs}(D[i], D[j], l)$}
			{\label{alg:optimizedAlgorithm:algorithmIIStart}
				$\overline{p} = \min\left\{ 1, \left( \frac{ w_{min}^2 2^{i + j  + 2} }{ W } \frac{ 1 }{ d(A,B)^d } \right)^\alpha \right\}$\;
				$r = geo\left( \overline{p} \right)$\;
				\While{$r \le \abs{A}\abs{B}$}{
					$u = A[\floor{ r / \abs{B} }]$\;
					$v = B[r \mod \abs{B}]$\;
					\If{$i \neq j \lor u < v$}{
						add $(u, v)$ to $E$ with probability $\frac{p_{uv}}{\overline{p}} = \frac{1}{\overline{p}}\min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}$\;
					}
					$r = r + geo\left( \overline{p} \right)$\;
				}
			}\label{alg:optimizedAlgorithm:algorithmIIEnd}
		}
	}

	\KwRet $E$ \;\label{alg:optimizedAlgorithm:algorithmIterEnd}
\end{algorithm}

\subsection{Complexity}

Now we will discuss complexity of presented solution to show, that it achieves required efficiency.

\begin{itemize}
	\item \sampleVertices{$n$, $\beta$, $w$}\\
		As it iterates over each of $n$ vertices and for each of $d$ dimension draws position, its complexity is $O(nd)$.

	\item \divideVerticesIntoLayers{$n$, $w$}\\
		It creates $ s = \floor{\log_2\left(\frac{w_{max}}{w_{min}}\right)} + 1 = \floor{\log_2\left(n^{\frac{1}{\beta-1}}\right)} + 1 = \frac{1}{\beta-1} \log_2(n) + O(1)$ layers and for each of $n$ vertices choose layer, in which it should be located, resulting in complexity $O(n)$.
	\item \divideLayersIntoCells{$d$, $L$, $x$}\\
		For any layer $L_i$ we have $l_i = \floor{\log_{2^d}\left( \frac{W}{2^{i+1} w_{min}^2} \right)}$.
		As $W = H_{n, \frac{1}{\beta - 1}} = n^{\frac{\beta - 2}{\beta - 1}} + O\left( 1 \right)$, (see Equation \ref{eq:asymptotics2}), we get: $l_i = \floor{\log_{2^d}(W) -\frac{i+1}{d} - \log_{2^d}(\delta^2)} = O\left(\log(n)\right)$.
		For each of $\frac{1}{\beta-1} \log_2(n) + O(1)$ layers we have:
		\begin{enumerate}
			\item
				initialization of temporary array of cells with length $2^{dl}$ (number of cells at level $l$) at line \ref{alg:divideLayersIntoCells:tempInitialisation}
			\item
				locating vertices in temporary array at line \ref{alg:divideLayersIntoCells:tempLocation}
			\item
				iterating over all cells at level $l$ at lines \ref{alg:divideLayersIntoCells:it1Start} - \ref{alg:divideLayersIntoCells:it1End}
			\item
				relocating vertices into their final locations at line \ref{alg:divideLayersIntoCells:relocation}
			\item
				iterating over all cells at levels up to $l-1$ at lines \ref{alg:divideLayersIntoCells:it2Start} - \ref{alg:divideLayersIntoCells:it2End}
		\end{enumerate}
		We can see, that points 2,3 and 5 are done once per each vertex through the whole function (as each vertex belongs only to one layer, and then to only one cell at the maximum level), so their complexity is $O(n)$.
		On the other hand steps 1, 4 and 6 depends only on number of cells, on which given layer is divided.
		Let us count number of cells at the maximum level $l$:
		\[2^{dl} = \left( 2^d \right)^{\left\lfloor\log_{2^d}\left( \frac{W}{2^{i+1} w_{min}^2} \right)\right\rfloor} \leq \frac{W}{2^{i+1} w_{min}^2},\]
		and number of cells at lower levels together:
		\[\sum_{i = 0}^{l-1} 2^{di} = \frac{2^{dl} - 1}{2^d - 1} \leq 2^{dl}.\]

		As we iterate over all s layers we get:
		\begin{equation*}\begin{split}
			\sum_{i = 1}^{ \frac{\log_2(n)}{\beta-1} + O(1)} \frac{W}{2^i w_{min}^2}
				= {} &  \frac{n^{\frac{\beta - 2}{\beta - 1}}}{\delta^2}\sum_{i = 1}^{ \frac{\log_2(n)}{\beta-1}  + O(1)} 2^{-i}
				\\\leq {} & \frac{n^{\frac{\beta - 2}{\beta - 1}}}{\delta^2} \frac{1 - (\frac{1}{2})^{\frac{1}{\beta - 1}\log_2(n) + O(1)}}{1 - \frac{1}{2}}
				\\= {} &  \frac{n^{\frac{\beta - 2}{\beta - 1}}}{2\delta^2} \left(1 - n^{-\frac{1}{\beta - 1}}O(1)\right)
				\\= {} &  \frac{n^{\frac{\beta - 2}{\beta - 1}}}{2\delta^2} + O\left( n^{\frac{\beta - 3}{\beta - 1}}\right)
		\end{split}\end{equation*}
		As $\beta \in (2,3)$, we have $n^{\frac{\beta - 2}{\beta - 1}} = O(\sqrt{n})$, so we have $O(\sqrt{n})$ cells generated together for all layers, and the whole function works in $O(n)$.

	\item \getNeighbours{$Id$, $D$, $L$}\\
		Its complexity is $O\left(3^d\right)$, as for $l > 1$ each cell has $3^d$ neighbours, which are returned. For $l = 0$ and $l=1$ number of neighbours is smaller (all cells at this levels, so respectively $1$ and $2^d$), so also fits in $O\left(3^d\right)$.

	\item \getTypeIPairs{$D_1$, $D_2$, $l$}\\
		This function loops over cells in $D_1$ at level $l$, and then over its neighbours in $D_2$.
		Number of cells at level $l$ equals $2^{ld}$, and number of neighbours is at most equal to $3^d$, giving complexity of $O\left(2^l 6^d\right)$.

	\item \getTypeIIPairs{$D_1$, $D_2$, $l$}\\
		For each $i\in \zeroTo{l-1}$ this function iterates over cells in $D_1$ at level $i$, then over its neighbours in $D_2$, and then over subcells of both of them, giving complexity $O\left(2^i 24^d\right)$ for one layer and  $O\left(\sum_{i = 0}^{l - 1}{2^i 24^d}\right) = O\left((2^l - 1)24^d\right)$ for layers up to $l$.

	\item whole Algorithm \ref{alg:optimizedAlgorithm}\\
		Let us divide it into preprocessing, generation of edges from cell pairs of type I, and from type II.
		First part consists of lines \ref{alg:optimizedAlgorithm:preprocStart} - \ref{alg:optimizedAlgorithm:preprocEnd}, and includes only functions, which are $O(n)$.
		Rest of the algorithm  (lines \ref{alg:optimizedAlgorithm:algorithmIterStart} - \ref{alg:optimizedAlgorithm:algorithmIterEnd}) contains iteration over product of previously constructed structures.
		To prove, that it is also $O(n)$, we will show that it works in $O(n + \E[m])$ and then use Theorem \ref{th:expectedEdges}.

		\begin{sloppypar}
		Firstly, for each pair $(A,B)$ of type I,  we compute probability $p_{uv} = \min\left\{ 1, \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha\right\}$.
		From the definition of type I pairs we have $d(A,B) = 0$, so $\dabs{x_u - x_v} \leq 2 (\frac{1}{2})^{l_{ij}}$, because distance between any two points in these cells is at most two times lengths of their edges.
		Then we get:
		$$
			\dabs{x_u - x_v}^d
				\leq 2^{d(1-l_{ij})}
				= 2^{d\left(1-\floor{\log_{2^d}\frac{W}{2^{i+j+2}\delta^2}}\right)}
				\leq 2^{d\left(2-\log_{2^d}\frac{W}{2^{i+j+2}\delta^2}\right)}
				= 4^d \frac{2^{i+j+2}\delta^2}{W}.
		$$
		Furthermore, as
			$2^i\delta \leq w_u < 2^{i+1}\delta $ and $2^j\delta \leq w_v < 2^{j+1}\delta $ (from the definition of layers $L_i$ and $L_j$),
		we get:
			$$ p_{uv} \geq \left(\frac{2^{i+j}}{W}2^{-2d} \frac{W}{2^{i+j+2}\delta^2}\right)^\alpha = 2^{-2\alpha(d+1)}.$$
		Let us denote by $N_{ij}^I = \abs{\{(u,v) \text{ generated from } \mathcal{D}_{ij}^I \}} = \sum_{(A,B)\in \mathcal{D}_{ij}^I}\abs{A}\abs{B}$.
		Then for every $0\leq i < j \leq s - 1$ we have lines \ref{alg:optimizedAlgorithm:algorithmIStart}-\ref{alg:optimizedAlgorithm:algorithmIEnd} working in $O(N_{ij}^I)$ time and expected number of generated edges
		\[E[m_{ij}^I] = \sum_{(u,v) \text{ generated from } \mathcal{D}_{ij}^I} p_{uv} \geq  2^{-2\alpha(d+1)} N_{ij}^I.\]
		Summing over all possible $i$ and $j$ we get:
		$$
			\E[m^I] \geq  2^{-2\alpha(d+1)} N^I,
		$$
		and algorithm working in time $O(N^I) = O(\E[m^I])$.
		\end{sloppypar}

		For pair $(A,B)$ of type II, in constant time we count probability bound in given cell $\overline{p} = \min\left\{ 1, \left(\frac{w_{min}^2 2^{i + j + 2}}{W}\frac{1}{d(A,B)^d}\right)^\alpha\right\}$.
		Then we repeatedly draw index of edge candidate from $geo\left( \overline{p} \right)$.
		We draw it at least once for each $(A,B)$ pair, and this gives $O(1)$ for drawing it for the last time, after which index is bigger than $\abs{A}\abs{B}$.
		For each former draw we obtain an edge candidate $(u,v)$, which becomes edge with probability $\frac{p_{u,v}}{\overline{p}}$.
		As:
		$$2^i\delta \leq w_u < 2^{i+1}\delta ,$$
		$$2^j\delta \leq w_v < 2^{j+1}\delta \text{ and}$$
		$$d(A,B) \leq \dabs{x_u - x_v} \leq d(A,B) + 2^{1-l} \leq 3 d(A,B)$$
		we get:
		$$
			\left(\frac{w_{min}^2 2^{i + j + 2}}{W}\frac{1}{d(A,B)^d}\right)^\alpha
			\geq \left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha
			\geq \left(\frac{1}{4\cdot3^d}\frac{w_{min}^2 2^{i + j + 2}}{W}\frac{1}{d(A,B)^d}\right)^\alpha
		$$
		When $\overline{p} < 1$ we get:
		$$
			\frac{p_{u,v}}{\overline{p}}
			= \frac{
				\left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha
			}{
				\left(\frac{w_{min}^2 2^{i + j + 2}}{W}\frac{1}{d(A,B)^d}\right)^\alpha
			}
			\geq \left(\frac{1}{4\cdot3^d}\right)^\alpha,
		$$ whether for $\overline{p} = 1$:
		$$
			\frac{p_{u,v}}{\overline{p}}
			= \min\left\{ 1,\left(\frac{w_u w_v}{W}\frac{1}{\dabs{x_u - x_v}^d}\right)^\alpha \right\}
			\geq \left(\frac{1}{4\cdot3^d}\right)^\alpha.
		$$

		Now by $N_{ij}^{II}$ we will denote edges candidates sampled from $\mathcal{P}_{ij}^{II}$ by geometric distribution.
		Then for every $0\leq i < j \leq s - 1$ we have lines \ref{alg:optimizedAlgorithm:algorithmIIStart}-\ref{alg:optimizedAlgorithm:algorithmIIEnd} working in $O(1 + N_{ij}^{II})$ time and expected number of generated edges
		\[E[m_{ij}^{II}] = \sum_{(u,v) \text{ generated from } \mathcal{D}_{ij}^{II}} p_{uv} \geq  \left(\frac{1}{4\cdot3^d}\right)^\alpha N_{ij}^{II}.\]
		Summing over all possible $i$ and $j$ we get:
		$$
			\E[m^{II}] \geq  \left(\frac{1}{4\cdot3^d}\right)^\alpha N^{II},
		$$
		and algorithm working in time $\sum_{0 \leq i \leq j \leq s}O(1 + N_{ij}^{II}) = O(n + N^{II}) = O(n + \E[m^{II}])$.

		In summary, the algorithm works in $O(n + \E[m^I] + \E[m^{II}]) = O(n + \E[m]) = O(n)$, where last equality follows from Theorem \ref{th:expectedEdges}.
\end{itemize}
