(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     25412,        659]
NotebookOptionsPosition[     24482,        623]
NotebookOutlinePosition[     24815,        638]
CellTagsIndexPosition[     24772,        635]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"girg", "[", 
   RowBox[{
   "n_", ",", "d_", ",", "\[Alpha]_", ",", "\[Beta]_", ",", "\[Delta]_", ",", 
    " ", "reps_"}], "]"}], ":=", 
  RowBox[{"ReadList", "[", 
   RowBox[{"StringJoin", "[", 
    RowBox[{
    "\"\<!/home/gumny/magisterka/src/cmake-build-release/girgGenerator \>\"", 
     ",", 
     RowBox[{"Riffle", "[", 
      RowBox[{
       RowBox[{"ToString", "/@", 
        RowBox[{"{", 
         RowBox[{
         "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]", ",", "\[Delta]", ",",
           " ", "reps"}], "}"}]}], ",", "\"\< \>\""}], "]"}]}], "]"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"girg", "[", 
   RowBox[{
   "n_", ",", "d_", ",", "\[Alpha]_", ",", "\[Beta]_", ",", "\[Delta]_"}], 
   "]"}], ":=", 
  RowBox[{"girg", "[", 
   RowBox[{
   "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]", ",", "\[Delta]", ",", " ", 
    "1"}], "]"}]}], "\n", 
 RowBox[{
  RowBox[{"statistic", "[", 
   RowBox[{
   "f_", ",", "numberOfExperiments_", ",", "n_", ",", "d_", ",", "\[Alpha]_", 
    ",", "\[Beta]_", ",", "\[Delta]_"}], "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"x", "=", 
     RowBox[{"Outer", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"#1", "[", "#2", "]"}], "&"}], ",", "f", ",", 
       RowBox[{"ParallelTable", "[", 
        RowBox[{
         RowBox[{"girg", "[", 
          RowBox[{
          "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]", ",", "\[Delta]"}], 
          "]"}], ",", 
         RowBox[{"{", "numberOfExperiments", "}"}]}], "]"}]}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"n", ",", 
        RowBox[{"Mean", "[", "#", "]"}], ",", 
        RowBox[{"Variance", "[", "#", "]"}]}], "}"}], "&"}], "/@", "x"}]}], 
   ")"}]}], "\n", 
 RowBox[{"ParameterString", ":=", 
  RowBox[{"StringJoin", "[", 
   RowBox[{"ToString", "/@", 
    RowBox[{"{", 
     RowBox[{
     "\"\<d = \>\"", ",", "d", ",", "\"\<, \[Alpha] = \>\"", ",", "\[Alpha]", 
      ",", "\"\<, \[Beta] = \>\"", ",", "\[Beta]", ",", 
      "\"\<, \[Delta] = \>\"", ",", "\[Delta]"}], "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.708017742401012*^9, 3.7080177424026537`*^9}, {
  3.708019255903207*^9, 3.708019281119117*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"d", "=", "3"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"\[Alpha]", "=", "20"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"\[Beta]", "=", "2.5"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"\[Delta]", "=", "1"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"reps", "=", "100"}], ";"}]}], "Input",
 CellChangeTimes->{{3.7080177932272987`*^9, 3.708017796101993*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DiscretePlot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"statistic", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"GroupOrder", "[", 
         RowBox[{"GraphAutomorphismGroup", "[", "#", "]"}], "]"}], "&"}], 
       "}"}], ",", "100", ",", "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]",
       ",", "\[Delta]"}], "]"}], "[", 
    RowBox[{"[", 
     RowBox[{"1", ",", "2"}], "]"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.708017832650456*^9, 3.708017905776537*^9}, {
  3.708018066080782*^9, 3.7080181698187923`*^9}, {3.708018209178515*^9, 
  3.708018213680386*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], LineBox[{},
       VertexColors->None]}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{1., 1.}, {1., 0}}, {{2., 2.}, {2., 0}}, {{3., 6.}, {
         3., 0}}, {{4., 24.}, {4., 0}}, {{5., 120.}, {5., 0}}, {{6., 
         672.96}, {6., 0}}, {{7., 2770.2}, {7., 0}}, {{8., 8672.16}, {
         8., 0}}, {{9., 19877.7}, {9., 0}}, {{10., 38496.78}, {10., 0}}},
       VertexColors->None]}}}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {}, 
    PointBox[{{1., 1.}, {2., 2.}, {3., 6.}, {4., 24.}, {5., 120.}, {6., 
     672.96}, {7., 2770.2}, {8., 8672.16}, {9., 19877.7}, {10., 
     38496.78}}], {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{1, 0},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "MessagesHead" -> DiscretePlot, "AxisPadding" -> Scaled[0.02], 
    "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> {
      Directive[
       RGBColor[0.368417, 0.506779, 0.709798], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.880722, 0.611041, 0.142051], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.560181, 0.691569, 0.194885], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.922526, 0.385626, 0.209179], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.528488, 0.470624, 0.701351], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.772079, 0.431554, 0.102387], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.363898, 0.618501, 0.782349], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[1, 0.75, 0], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.647624, 0.37816, 0.614037], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.571589, 0.586483, 0.], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.915, 0.3325, 0.2125], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
       AbsoluteThickness[1.6]]}, "DomainPadding" -> Scaled[0.02], 
    "RangePadding" -> Scaled[0.05]},
  PlotRange->{{1, 10}, {0, 38496.78}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.7080181149714737`*^9, 3.7080181483745327`*^9}, {
   3.708018209338121*^9, 3.708018224047422*^9}, 3.708019326106822*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"10", "!"}]], "Input",
 CellChangeTimes->{{3.708076621822637*^9, 3.708076622293058*^9}}],

Cell[BoxData["3628800"], "Output",
 CellChangeTimes->{3.708076623599339*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"girg", "[", 
  RowBox[{"10", ",", "d", ",", "\[Alpha]", ",", "\[Beta]", ",", "1"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.708076650690074*^9, 3.708076686941869*^9}, {
  3.708076723765398*^9, 3.708076724718614*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  GraphicsBox[
   NamespaceBox["NetworkGraphics",
    DynamicModuleBox[{Typeset`graph = HoldComplete[
      Graph[{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, {
       Null, {{1, 4}, {1, 6}, {1, 7}, {1, 8}, {1, 5}, {1, 9}, {1, 10}, {1, 
        2}, {1, 3}, {2, 4}, {2, 6}, {2, 7}, {2, 8}, {2, 5}, {2, 9}, {2, 10}, {
        2, 3}, {3, 4}, {3, 6}, {3, 7}, {3, 8}, {3, 5}, {3, 9}, {3, 10}, {4, 
        6}, {4, 7}, {4, 8}, {4, 5}, {4, 9}, {4, 10}, {5, 6}, {5, 7}, {5, 8}, {
        5, 10}, {6, 7}, {6, 8}, {6, 9}, {7, 8}, {7, 10}, {9, 10}}}, {
       GraphLayout -> "CircularEmbedding", GridLinesStyle -> Directive[
          GrayLevel[0.5, 0.4]]}]]}, 
     TagBox[
      GraphicsGroupBox[
       GraphicsComplexBox[{{-0.5877852522924737, 
        0.809016994374948}, {-0.9510565162951538, 
        0.3090169943749484}, {-0.9510565162951534, -0.30901699437494645`}, \
{-0.5877852522924726, -0.8090169943749468}, {6.049014748177263*^-16, -1.}, {
        0.5877852522924738, -0.8090169943749481}, {
        0.9510565162951539, -0.3090169943749485}, {0.9510565162951533, 
        0.30901699437494634`}, {0.5877852522924726, 
        0.8090169943749468}, {-7.044813998280222*^-16, 1.}}, {
         {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
          {Arrowheads[0.], ArrowBox[{1, 2}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 3}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 4}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 5}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 6}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 9}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{1, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 3}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 4}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 5}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 6}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 9}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{2, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 4}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 5}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 6}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 9}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{3, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 5}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 6}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 9}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{4, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{5, 6}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{5, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{5, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{5, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{6, 7}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{6, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{6, 9}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{7, 8}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{7, 10}, 0.02261146496815286]}, 
          {Arrowheads[0.], ArrowBox[{9, 10}, 0.02261146496815286]}}, 
         {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[0.7]}], 
          DiskBox[1, 0.02261146496815286], DiskBox[2, 0.02261146496815286], 
          DiskBox[3, 0.02261146496815286], DiskBox[4, 0.02261146496815286], 
          DiskBox[5, 0.02261146496815286], DiskBox[6, 0.02261146496815286], 
          DiskBox[7, 0.02261146496815286], DiskBox[8, 0.02261146496815286], 
          DiskBox[9, 0.02261146496815286], 
          DiskBox[10, 0.02261146496815286]}}]],
      MouseAppearanceTag["NetworkGraphics"]],
     AllowKernelInitialization->False]],
   DefaultBaseStyle->{
    "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
   FrameTicks->None,
   GridLinesStyle->Directive[
     GrayLevel[0.5, 0.4]]], "}"}]], "Input",
 CellChangeTimes->{{3.708076740633459*^9, 3.708076749000682*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DiscretePlot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"statistic", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"GroupOrder", "[", 
         RowBox[{"GraphAutomorphismGroup", "[", "#", "]"}], "]"}], "&"}], 
       "}"}], ",", "100", ",", "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]",
       ",", "\[Delta]"}], "]"}], "[", 
    RowBox[{"[", 
     RowBox[{"1", ",", "2"}], "]"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "10", ",", "100", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.708019988008889*^9, 3.708019992255896*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{40., 1.06}, {40., 1.06}}},
       VertexColors->None]}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{10., 1790.8}, {10., 1.06}}, {{20., 2.96}, {20., 1.06}}, {{
       30., 1.35}, {30., 1.06}}, {{50., 1.07}, {50., 1.06}}, {{60., 1.18}, {
       60., 1.06}}, {{70., 1.11}, {70., 1.06}}, {{80., 1.13}, {80., 1.06}}, {{
       90., 1.11}, {90., 1.06}}, {{100., 1.13}, {100., 1.06}}},
       VertexColors->None]}}}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {}, 
    PointBox[{{10., 1790.8}, {20., 2.96}, {30., 1.35}, {40., 1.06}, {50., 
     1.07}, {60., 1.18}, {70., 1.11}, {80., 1.13}, {90., 1.11}, {100., 
     1.13}}], {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{10, 1.06},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "MessagesHead" -> DiscretePlot, "AxisPadding" -> Scaled[0.02], 
    "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> {
      Directive[
       RGBColor[0.368417, 0.506779, 0.709798], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.880722, 0.611041, 0.142051], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.560181, 0.691569, 0.194885], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.922526, 0.385626, 0.209179], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.528488, 0.470624, 0.701351], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.772079, 0.431554, 0.102387], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.363898, 0.618501, 0.782349], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[1, 0.75, 0], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.647624, 0.37816, 0.614037], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.571589, 0.586483, 0.], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.915, 0.3325, 0.2125], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
       AbsoluteThickness[1.6]]}, "DomainPadding" -> Scaled[0.02], 
    "RangePadding" -> Scaled[0.05]},
  PlotRange->{{10, 100}, {1.06, 5.795}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.708020028869504*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DiscretePlot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"statistic", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"GroupOrder", "[", 
         RowBox[{"GraphAutomorphismGroup", "[", "#", "]"}], "]"}], "&"}], 
       "}"}], ",", "100", ",", "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]",
       ",", "\[Delta]"}], "]"}], "[", 
    RowBox[{"[", 
     RowBox[{"1", ",", "2"}], "]"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "100", ",", "1000", ",", "100"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.708019333739726*^9, 3.708019336179735*^9}, {
  3.708019383007515*^9, 3.7080193833853493`*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{100., 1.14}, {100., 1.14}}},
       VertexColors->None]}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{200., 1.31}, {200., 1.14}}, {{300., 1.61}, {300., 1.14}}, {{
       400., 1.92}, {400., 1.14}}, {{500., 2.15}, {500., 1.14}}, {{600., 
       2.56}, {600., 1.14}}, {{700., 3.73}, {700., 1.14}}, {{800., 2.9}, {
       800., 1.14}}, {{900., 2.99}, {900., 1.14}}, {{1000., 3.8}, {1000., 
       1.14}}},
       VertexColors->None]}}}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {}, 
    PointBox[{{100., 1.14}, {200., 1.31}, {300., 1.61}, {400., 1.92}, {500., 
     2.15}, {600., 2.56}, {700., 3.73}, {800., 2.9}, {900., 2.99}, {1000., 
     3.8}}], {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{100, 1.14},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "MessagesHead" -> DiscretePlot, "AxisPadding" -> Scaled[0.02], 
    "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> {
      Directive[
       RGBColor[0.368417, 0.506779, 0.709798], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.880722, 0.611041, 0.142051], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.560181, 0.691569, 0.194885], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.922526, 0.385626, 0.209179], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.528488, 0.470624, 0.701351], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.772079, 0.431554, 0.102387], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.363898, 0.618501, 0.782349], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[1, 0.75, 0], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.647624, 0.37816, 0.614037], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.571589, 0.586483, 0.], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.915, 0.3325, 0.2125], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
       AbsoluteThickness[1.6]]}, "DomainPadding" -> Scaled[0.02], 
    "RangePadding" -> Scaled[0.05]},
  PlotRange->{{100, 1000}, {1.14, 3.8}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.708019380026328*^9, 3.708019431147138*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DiscretePlot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"statistic", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"GroupOrder", "[", 
         RowBox[{"GraphAutomorphismGroup", "[", "#", "]"}], "]"}], "&"}], 
       "}"}], ",", "100", ",", "n", ",", "d", ",", "\[Alpha]", ",", "\[Beta]",
       ",", "\[Delta]"}], "]"}], "[", 
    RowBox[{"[", 
     RowBox[{"1", ",", "2"}], "]"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1000", ",", "10000", ",", "1000"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.70801949153125*^9, 3.708019493399538*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], LineBox[{},
       VertexColors->None]}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
      0.019444444444444445`], AbsoluteThickness[1.6], Opacity[0.2], 
      LineBox[{{{1000., 5.41}, {1000., 0}}, {{2000., 64.73}, {2000., 0}}, {{
         3000., 111.65}, {3000., 0}}, {{4000., 920.54}, {4000., 0}}, {{5000., 
         2718.4}, {5000., 0}}, {{6000., 7420.04}, {6000., 0}}, {{7000., 
         39041.12}, {7000., 0}}, {{8000., 1.24315392*^6}, {8000., 0}}, {{
         9000., 2.22037504*^6}, {9000., 0}}, {{10000., 1.43393024*^6}, {
         10000., 0}}},
       VertexColors->None]}}}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], {}, 
    PointBox[{{1000., 5.41}, {2000., 64.73}, {3000., 111.65}, {4000., 
     920.54}, {5000., 2718.4}, {6000., 7420.04}, {7000., 39041.12}, {8000., 
     1.24315392*^6}, {9000., 2.22037504*^6}, {10000., 1.43393024*^6}}], {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{1000, 0},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "MessagesHead" -> DiscretePlot, "AxisPadding" -> Scaled[0.02], 
    "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> {
      Directive[
       RGBColor[0.368417, 0.506779, 0.709798], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.880722, 0.611041, 0.142051], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.560181, 0.691569, 0.194885], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.922526, 0.385626, 0.209179], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.528488, 0.470624, 0.701351], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.772079, 0.431554, 0.102387], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.363898, 0.618501, 0.782349], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[1, 0.75, 0], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.647624, 0.37816, 0.614037], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.571589, 0.586483, 0.], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.915, 0.3325, 0.2125], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.40082222609352647`, 0.5220066643438841, 0.85], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.9728288904374106, 0.621644452187053, 0.07336199581899142], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.736782672705901, 0.358, 0.5030266573755369], 
       AbsoluteThickness[1.6]], 
      Directive[
       RGBColor[0.28026441037696703`, 0.715, 0.4292089322474965], 
       AbsoluteThickness[1.6]]}, "DomainPadding" -> Scaled[0.02], 
    "RangePadding" -> Scaled[0.05]},
  PlotRange->{{1000, 10000}, {0, 2.22037504*^6}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.708019898318453*^9}]
}, Open  ]]
},
WindowSize->{1280, 705},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 2266, 64, 187, "Input"],
Cell[2827, 86, 369, 11, 121, "Input"],
Cell[CellGroupData[{
Cell[3221, 101, 694, 18, 32, "Input"],
Cell[3918, 121, 3305, 82, 229, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7260, 208, 111, 2, 32, "Input"],
Cell[7374, 212, 76, 1, 32, "Output"]
}, Open  ]],
Cell[7465, 216, 242, 5, 32, "Input"],
Cell[7710, 223, 4792, 78, 205, InheritFromParent],
Cell[CellGroupData[{
Cell[12527, 305, 607, 16, 32, "Input"],
Cell[13137, 323, 3260, 82, 244, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16434, 410, 664, 18, 32, "Input"],
Cell[17101, 430, 3313, 83, 236, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20451, 518, 615, 17, 32, "Input"],
Cell[21069, 537, 3397, 83, 213, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
