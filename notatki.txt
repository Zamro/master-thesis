04.11.2016
	1. Zaimplementować jeden z dwóch algorytmów przedstawionych w pracy o entropii - obojętnie który, ich wynik powinien być taki sam, złożoność nas aż tak nie interesuje

	2. Zaimplementować generator grafów GIRG

	3. Przepuszczać przez algorytm kompresji i patrzyć, jak dobrze działa

	4. jak się ma entropia ze slajdów do entropii tej z pracy - czy to to samo, czy coś innego?

	5. postarać się policzyć entropię strukturalną grafu GIRG:
		wykryć symetrie


	13.11-16.12 - kontakt tylko przez maila

07.03.2017
	Algorytmy kompresji - szybszy algorytm działa tylko dla G(n,p) - przyspieszenie opiera się na znanym prawdopodobieństwie występowania danej krawędzi (3.2 Theorem 2 (iii) )


	GroupOrder[GraphAutomorphismGroup[[]]

	dokończyć binary arithmetic encoder
	generować GIRGi, sprawdzać w Mathematice
	doczytać o kompresji GIRGów

05.04.2017
	Poza implementacją generatorów GIRGów i sprawdzeniem doświadczalnym, spojrzeć na dowody asymetrii GNP z pracy Szpankowskiego i cytowanej w niej pracy kogoś innego.

17.05.2017
	- sprawdzić, czy mogą być pętle do samego siebie.
	- dokładniejszy opis generatora grafów może stanowić dużą cześć pracy 5-10 stron).

	Seminarium:
		15 minut
		10-15 slajdów
		mało tekstu
		raczej unikać efektów graficznych, chyba że mega profesjonalnie
		itemy w beamerze spoko, ale też nie przesadzać
		zdrowy rozsądek z obrazkami
			twierdzenie Stokesa jest spoko obrazkiem xD

24.05.2017
	1. Przeanalizować o co im chodzi w 2 - 3 bitach na krawędź
	Co warto narysować:
		jak zachowują się stopnie wierzchołków?
		dlaczego d = 3 jest dziwne?

	2. str 15 z prezentacji - spróbować policzyć E[deg(v_i)] bez thety, użyć prawdopodobieństwo takie jak użyłem.
	3. Spróbować udowodnić coś z asymetrycznością.

31.05.2017
	przeliczyć wszystko na spokojnie, najlepiej zapisywać do texa i podsyłać.
	doprowadzić do jakiejś formy, która zgadza się z wyglądem grafów
	spróbować narysować stopnie, zobaczyć, jak się układają - powinno być O(w_v), może dowód asymetryczności na tym oprzeć, tak jak w preferencial atachment graphs.
	wpisywać fragmenty do TEXa tak, żeby zaczęła powstawać praca
	zastąpienie funkcji przez rozkład może nam ułatwić liczenie
	Jensen Inequality - spojrzeć, może coś pomoże


07.06.2017
	może coś ciekawego o odległości w przestrzeni (podobnie jak na wykładzie Cichonia o kuli w hiperkostce)

	Razem na pracę magisterską powinny wystarczyć:
	*stopnie
	*zbliżanie wierzchołków przez zwiększanie wymiaru
	*opis generatorów, porównanie złożoności, wnioski z wykresów

	~20 stron wystarczające

	Future works:
		asymetria
		entropia

	poniedziałki 11:30 - 16:00 - napisać maila, skontaktować się z Pawłem i Piotrkiem

12.06.2017
	zaznaczyć, że X_uv jest zmienną mieszaną
	narysować może wykres X_uv, żeby było widać, jak jest mieszana
	policzyć całkę przez części - wtedy wyjdzie ładnie bez 1
	liczenie sumy - w Mathematice, jakby było pytanie na obronie, to pamiętać, że jest Euler Sumation Formula

	w piątek albo w sobotę może na 2 godziny uda się spotkać (Costa Cofee w Renomie?)
	dać znać w piątek rano, czy będę coś miał

	
	priorytety:
		doliczyć
		poprzeć symulacjami
		opisać, co mam (model, algorytm generowania)
		zasugerować, że może być asymetryczny
		"płynnie przejść" do algorytmu kompresji Szpankowskiego, odnieść się do 2-3 bitów na krawędź
		
	rozdziały:
		wstęp
		model
		algorytm generowania
		wyliczenia E[deg]
		asymetryczność
		użycie algorytmu Szpankowskiego
		wnioski, future works, co jeszcze można z tym zrobić

26.06.2017
	temat: Analiza niejednorodnych losowych grafów geometrycznych
	ogólne:
		większe akapity - nie po jednym zdaniu		
	2.
		2.1: 
			random weights - tutaj można spróbować policzyć, czy są niezależne, nie widać na pierwszy rzut oka, czy są, czy nie
	3.
		3.2:
			skrócić wielkie zdanie!
			może |E| zamiast m
			notation dodać na początku			
		pseudokody podzielić na funkcje
	4.
		u^* + 1 w granicy sumy poprawić
		Wyliczyć najpierw najważniejszy czynnik, potem wyliczać resztę - wtedy będzie powód, dla którego można brać
			O(pierwszego czynnika)
		ponazywać części równań tak, żeby potem się do nich odnosić po nazwach
		poskładać niektóre części w jedną linię ( u^* na przykład )

21.08.2017
done:
	- w simple algorithm u+1
	- at THE lowest
	- if more than one vertex/vertices??
todo:
	- uciąć oś od 0 do 1
	- przytoczyć Le Cam's theorem
	- może przenieść rozdział algorytm za dowody
	- jaka jest wartość oczekiwana liczby węzłów na kolejnych warstwach? - pytanie, które nasuwa się przy czytaniu
	- napisać dokładniej, że mamy layery, które dzielimy na celki, które są na różnych levelach.
	- lowest/higher level - sprecyzować i się trzymać.
	- zapisywać x jako wektor
	- zdanie z drzewem - nie wiadomo jakie drzewo
	- continuously enumerated - czy to poprawne?
	- poprawić zdanie o wybraniu dwóch cellek, par typu I i typu II, bo nie wiadomo do końca o co chodzi 
	- d(A,B) dopisać jakoś, niekoniecznie programistycznie, subcells też opisać, może przy opisie piramidy/drzewa
	- napisać o całym algorytmie
	- (rozdział 4.) twierdzenie, potem dowód wyniku, po drodze lematy, napisać, że na dowód składają się lematy takie takie i takie wraz z dowodami
	
04.09.2017
	sec. 3.0 - uniknąć expected probability? - Probability of connecting
	zero-jedynkowa więc wartość oczekiwana = Pr, wpisać E[X_uv]
	3.2 - delikatnie napisać, że to jest E[Xuv|wv,wu,xv]
	
	Summary pierwsze zdanie