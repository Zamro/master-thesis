#pragma once
#include <vector>
#include <string>
#include <fstream>

struct PlotRecord
{
	std::string name;
	std::vector<double> y;
	bool lines;
};

struct PlotData
{
	std::string title;
	std::vector<double> x;
	std::vector<PlotRecord> records;
	std::string output;
	std::string xAxisName;
	std::string yAxisName;
	std::string additionalPlots;
};

void plot(const PlotData& data)
{
	system("mkdir -p out");
	std::string tempFileName = data.output + ".txt";
	std::ofstream tempFile("out/" + tempFileName);
	for(unsigned i = 0 ; i < data.x.size(); i++)
	{
		tempFile << data.x[i];
		for(const auto& record : data.records)
			tempFile << " " << record.y[i];
		tempFile << "\n";
	}
	tempFile.close();
	
	std::string gpFileName = data.output + ".gp";
    std::ofstream out("out/" + gpFileName);
    out<<"set key left\n"
	   <<"set title \""<<data.title<<"\"\n"
       <<"set xlabel \""+data.xAxisName+"\"\n"
       <<"set ylabel \""+data.yAxisName+"\"\n"
       <<"set terminal png enhanced "
//       <<"size 1280, 1024"
       <<"\n"
       <<"set output \"" + data.output + ".png\"\n"
       <<"plot ";
	for(unsigned i = 0; i< data.records.size(); i++)
	{
		const auto& record = data.records[i];
		out <<" \"" << tempFileName
			<< "\" using 1:" << i + 2 << (record.lines?" with lines lw 2":" ps 0.7") << " title '" << record.name + "',\\\n";
	}
	out << data.additionalPlots <<"\n";
    out.close();
    system(("pushd out  > /dev/null; gnuplot " + gpFileName + "; popd  > /dev/null").c_str());
}
