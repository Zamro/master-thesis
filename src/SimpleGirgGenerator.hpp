#pragma once
#include <random>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <functional>
#include <memory>
#include <iostream>
#include <utility>
#include "Graph.hpp"
#include "Vertex.hpp"

class SimpleGirgGenerator{

public:
    static Graph generate(unsigned n, unsigned dimension, ValueType alpha, ValueType beta, ValueType delta)
    {
        Graph out{n};

        std::vector<Vertex> vertices;
        for(unsigned i=0; i<n; i++)
            vertices.push_back(Vertex(i, n, dimension, beta, delta));

        std::vector<double> weights;
        weights.resize(vertices.size());
        transform(vertices.begin(), vertices.end(), weights.begin(), [](const Vertex& vertex){return vertex.weight;});
        double sumOfWeights = accumulate(weights.begin(),
                                  weights.end(),
                                  0.);
        for(Vertex& u : vertices)
            for(Vertex& v : vertices)
                if(u.index < v.index)
                    if(tossIfShouldBeConnected(u, v, sumOfWeights, alpha))
                        out.putEdge(u.index, v.index);
         return out;
    }
};
