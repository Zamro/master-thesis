//
// Created by Pawel on 03.04.2017.
//

#pragma once

#include <vector>

using Bitstring = std::vector<bool>;
struct BinaryEncoding{
    Bitstring bits;
    long unsigned length;
};