#include "Graph.hpp"

Graph::Graph(unsigned n):
	n(n)
{
	for(unsigned i = 0; i < n; i++)
		nodes.push_back({i,{}});
}

bool Graph::operator==(const Graph& other)
{
	if (not n == other.n)
		return false;
	for (int i = 0; i < n; i++)
	{
		if(nodes[i].neighboursList != other.nodes[i].neighboursList)
			return false;
	}
	return true;
}

void Graph::putEdge(int i, int j)
{
	nodes[i].neighboursList.push_back(j);
	nodes[j].neighboursList.push_back(i);
}

Node& Graph::node(int i)
{
	return nodes[i];
}

const Node& Graph::node(int i) const
{
	return nodes[i];
}

std::ostream& operator<<(std::ostream& out, const Graph& graph)
{
	int edgeCounter = 0;
	out<<"Graph:";
	for(auto node : graph.nodes)
	{
		out<<std::endl;
		out<<"\t node: "<< node.id<<" neighbours : ";
		for(auto neighbourId : node.neighboursList)
			out<<neighbourId<<" ", edgeCounter++;
	}
	unsigned long n = graph.nodes.size();
	double maxNumOfEdges = n*(n-1);
	out<<std::endl<<"mean p:"<<edgeCounter/maxNumOfEdges;
	return out;
}

unsigned Graph::getNumberOfEdges() const {
    unsigned sum{};
	for(const auto& node: nodes)
		sum += node.neighboursList.size();
	return sum / 2;
}

std::ostream& operator<<(std::ostream& out, const MathematicaGraphWriter& graph)
{
	const auto& nodes = graph.graph.nodes;
	out<<"Graph[{";
	for(unsigned i = 0; i<nodes.size(); ++i)
	{
		if(i>0)
			out<<", ";
		out<<i;
	}
	out<<"}, {";
	bool isFirstWritten = 0;
	for(unsigned i = 0; i<nodes.size(); ++i)
	{
		const auto& node = nodes[i];
		for(unsigned j = 0; j<node.neighboursList.size(); ++j)
		{
			if(node.neighboursList[j] > node.id)
			{
				if(isFirstWritten)
					out<<", ";
				out<<node.id<<"<->"<<node.neighboursList[j];
				isFirstWritten = true;
			}
		}

	}
	out<<"}]";
	return out;
}
