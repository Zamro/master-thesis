//
// Created by Pawel on 20.05.2017.
//
#pragma once
#include <random>
#include <chrono>
#include <algorithm>
#include <functional>
#include <memory>
#include <iostream>
#include "Vertex.hpp"
#include "GirgCells.hpp"

class VerticesInCellsGeometricOrdering{
    unsigned cellsLevel;
    std::vector<Vertex> continuousVertices;
    std::vector<std::vector<ContinuousCell*>> indexToCellMappings;
    std::unique_ptr<ContinuousCell> cell;
public:
    VerticesInCellsGeometricOrdering(const std::vector<Vertex>& vertices, unsigned dimension, unsigned maximumCellsLevel);
    std::vector<ContinuousCell*> getNeighbourCells(const Position& position, unsigned cellLevel);
    std::vector<ContinuousCell*>& getCellsAtLevel(unsigned level);
    
private:
    std::unique_ptr<ContinuousCell> generateCells(const std::vector<Vertex>& vertices, unsigned dimension);
    std::vector<std::vector<ContinuousCell *>> initializeIndexToCellMapping(unsigned int dimension);
};

