#pragma once

#include <random>
#include <vector>
#include <chrono>
#include <iostream>

using ValueType = double;
using Position = std::vector<ValueType>;

extern std::default_random_engine generator;

struct Vertex{
	static std::uniform_real_distribution<ValueType> dist;
	unsigned index;
    Position x;
    ValueType weight;
    Vertex(unsigned i, unsigned n, unsigned d, double beta, double delta);
    Vertex(unsigned i, Position x, ValueType weight);
};

unsigned dimensionDependentRounding(ValueType x, unsigned dimension);
bool tossCoinWithGivenProbability(ValueType p);
ValueType distanceOnCircle(ValueType x, ValueType y);
ValueType distanceOnTorus(const Position& x, const Position& y);
ValueType probabilityOfConnecting(const Vertex &u, const Vertex &v, ValueType totalWeight);
bool tossIfShouldBeConnected(const Vertex &u, const Vertex &v, ValueType totalWeight, ValueType alpha, ValueType probabilityDenominator = 1.0);
unsigned positionToIndex(const Position&, unsigned cellLevel);
std::vector<unsigned> getNeighbourIndexes(unsigned index, unsigned cellLevel, unsigned maxDimension);

void writePosition(const Position& pos);