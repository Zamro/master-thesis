//
// Created by Pawel on 20.05.2017.
//

#include <list>
#include "VerticesInCellsGeometricOrdering.hpp"

VerticesInCellsGeometricOrdering::VerticesInCellsGeometricOrdering(const std::vector<Vertex>& vertices,
                                                                   unsigned dimension, unsigned maximumCellsLevel) :
        cellsLevel(maximumCellsLevel),
        continuousVertices{},
        indexToCellMappings(initializeIndexToCellMapping(dimension)),
        cell(generateCells(vertices, dimension))
{
}

std::vector<std::vector<ContinuousCell*>>
VerticesInCellsGeometricOrdering::initializeIndexToCellMapping(unsigned dimension)
{
    std::vector<std::vector<ContinuousCell*>> out(cellsLevel + 1);
    for(unsigned i = 0; i <= cellsLevel; i++)
    {
        indexToCellMappings[i].resize(1u<<(dimension * i));
    }
    return out;
}

std::unique_ptr<ContinuousCell>
VerticesInCellsGeometricOrdering::generateCells(const std::vector<Vertex>& vertices, unsigned dimension) {
    indexToCellMappings.resize(cellsLevel + 1);
    continuousVertices.reserve(vertices.size());
    auto cells = std::make_unique<ContinuousCell>(cellsLevel, dimension);
    std::vector<std::list<Vertex>> temporaryCells(1u<<(dimension*cellsLevel));
    for(auto vertex : vertices)
        temporaryCells[positionToIndex(vertex.x, cellsLevel)].push_back(vertex);
    cells->setVertices(continuousVertices, indexToCellMappings, temporaryCells);
    return cells;
}

std::vector<ContinuousCell*>
VerticesInCellsGeometricOrdering::getNeighbourCells(const Position& position, unsigned cellLevel) {
    std::vector<ContinuousCell*> out{};
    for(auto index : getNeighbourIndexes(positionToIndex(position, cellLevel),
                                         cellLevel,
                                         unsigned(position.size())))
        out.push_back(indexToCellMappings[cellLevel][index]);
    return out;
}

std::vector<ContinuousCell*>& VerticesInCellsGeometricOrdering::getCellsAtLevel(unsigned level) {
    return indexToCellMappings[level];
}
