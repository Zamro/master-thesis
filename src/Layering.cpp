//
// Created by Pawel on 23.04.2017.
//

#include "Layering.hpp"

Layering::Layering(std::vector<Vertex>& vertices) {
    std::vector<double> weights;
    weights.resize(vertices.size());
    transform(vertices.begin(), vertices.end(), weights.begin(), [](const Vertex& vertex){return vertex.weight;});

    auto wminmax = minmax_element(weights.begin(),
                                  weights.end());
    wmin = *wminmax.first;
    wmax = *wminmax.second;
    sumOfWeights = accumulate(weights.begin(),
                              weights.end(),
                              0.);

    unsigned maxIndex = (unsigned)log2(wmax / wmin);
    for(unsigned i = 0; i<=maxIndex; i++)
        limits.push_back(std::ldexp(wmin,i));

    layers.resize(limits.size());
    limits.push_back(2 * limits.back());

    for(Vertex& vertex : vertices)
    {
        unsigned index = (unsigned)log2(vertex.weight / wmin);
        layers[index].push_back(vertex);
    }
}
