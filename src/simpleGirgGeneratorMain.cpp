#include <iostream>
#include "Graph.hpp"
#include "SimpleGirgGenerator.hpp"

void displayHelp(char* exeName)
{
    std::cerr<<"Usage:\n\t"<<exeName<<" n d alpha beta delta\n";
    std::cerr<<"\t\tn - number of vertices - positive integer\n";
    std::cerr<<"\t\td - number of dimensions for torus - positive integer\n";
    std::cerr<<"\t\talpha - model parameter, real value > 1\n";
    std::cerr<<"\t\tbeta - model parameter, real value, 2 > beta > 3\n";
    std::cerr<<"\t\tdelta - model parameter, real value > 0, it's small value (< 0.1) can cause slow performance\n";
    std::cerr<<"\t\t[repeats] - number of generated graphs, default value = 1\n";
}


int main(int argc, char* argv[])
{
    std::ios_base::sync_with_stdio(0);
    std::cerr<<"SimpleGirgGenerator: reading parameters\n";
    if(argc < 6)
    {
        displayHelp(argv[0]);
        return 1;
    }

    unsigned n = unsigned(std::stoul(argv[1], nullptr, 0));
    unsigned d = unsigned(std::stoul(argv[2], nullptr, 0));
    double alpha = std::stod(argv[3], nullptr);
    double beta = std::stod(argv[4], nullptr);
    double delta = std::stod(argv[5], nullptr);
    unsigned reps = argc>=7?unsigned(std::stoul(argv[6], nullptr, 0)):1;

    std::cerr<<"SimpleGirgGenerator: parameters read: \nn= "<<n<<"\nn= "<<d<<"\nalpha= "<<alpha<<"\nbeta= "<<beta<<"\ndelta= "<<delta<<", generating graph\n";
    for(unsigned i =0; i<reps;i++)
    {
        Graph graph = SimpleGirgGenerator::generate(n, d, alpha, beta, delta);
        std::cerr<<"SimpleGirgGenerator: graph generated, writing to output\n";
        std::cout << MathematicaGraphWriter(graph)<<"\n";
    }
    std::cerr<<"SimpleGirgGenerator: finshed\n";
}