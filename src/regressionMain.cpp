#include <iostream>
#include <cmath>
#include <cassert>

#include "Graph.hpp"
#include "graphCompression.hpp"
#include "BinaryArithmeticCoding.hpp"
#include "GirgGenerator.hpp"
#include "GirgCells.hpp"

Graph exampleGraph()
{
    Graph out(10);
    out.putEdge(0, 3);
    out.putEdge(0, 4);
    out.putEdge(1, 2);
    out.putEdge(1, 5);
    out.putEdge(2, 3);
    out.putEdge(2, 5);
    out.putEdge(2, 6);
    out.putEdge(2, 7);
    out.putEdge(3, 4);
    out.putEdge(3, 5);
    out.putEdge(3, 7);
    out.putEdge(3, 8);
    out.putEdge(4, 6);
    out.putEdge(4, 7);
    out.putEdge(5, 6);
    out.putEdge(5, 7);
    out.putEdge(5, 8);
    out.putEdge(5, 9);
    out.putEdge(6, 7);
    out.putEdge(6, 8);
    out.putEdge(6, 9);
    out.putEdge(7, 9);
    out.putEdge(8, 9);
    return out;
}

Graph decompressedGraph()
{
    Graph out(10);
    out.putEdge(0, 1);
    out.putEdge(0, 2);
    out.putEdge(1, 2);
    out.putEdge(1, 3);
    out.putEdge(1, 4);
    out.putEdge(1, 5);
    out.putEdge(1, 6);
    out.putEdge(2, 3);
    out.putEdge(2, 7);
    out.putEdge(3, 4);
    out.putEdge(3, 5);
    out.putEdge(3, 7);
    out.putEdge(3, 8);
    out.putEdge(4, 5);
    out.putEdge(4, 7);
    out.putEdge(4, 9);
    out.putEdge(5, 6);
    out.putEdge(5, 7);
    out.putEdge(5, 8);
    out.putEdge(5, 9);
    out.putEdge(6, 7);
    out.putEdge(6, 8);
    out.putEdge(7, 8);
    return out;
}

void writeBitstringToStdcerr(const Bitstring& bitstring)
{
    for(auto b:bitstring) std::cerr<<b; std::cerr<<std::endl;
}

double binaryArithmeticCodingTest(const Bitstring &bitstring, bool verbose = 0)
{
    BinaryArithmeticCoding coder;
    auto code = coder.encode(bitstring);
    BinaryArithmeticCoding decoder;
    auto decoded = decoder.decode(code);
    if(not std::equal(bitstring.begin(), bitstring.end(), decoded.begin()) or verbose)
    {
        std::cerr<<"---- TEST ---\n";
        writeBitstringToStdcerr(bitstring);
        std::cerr<<"\t"<<"ERROR"<<std::endl;
        std::cerr<<"\tCompressed "<<bitstring.size()<< " to "<<code.bits.size()<<" bits\n";
        writeBitstringToStdcerr(code.bits);
        writeBitstringToStdcerr(decoded);
    }
    return bitstring.size() / double(code.bits.size());
}

std::vector<Bitstring> binaryArithmeticCodingTests(int n)
{
    std::vector<Bitstring> out{};
    if(n <= 1)
    {
        out = {{0},{1}};
    }
    else
    {
        std::vector<Bitstring> previous = binaryArithmeticCodingTests(n - 1);
        for(auto& bitstring : previous)
        {
            out.push_back(bitstring);
            out.back().push_back(0);
            out.push_back(std::move(bitstring));
            out.back().push_back(1);
            if(out.size() > 100)
                break;
        }
    }
    double ratiosSum{};
    int counter{};
    for(auto bitstring : out)
    {
        try{
            ratiosSum += binaryArithmeticCodingTest(bitstring);
            counter++;
        }catch(std::underflow_error e)
        {
            std::cerr<<"ERROR: "<<e.what()<<"\n";
            std::cerr<<"when compressing: ";
            writeBitstringToStdcerr(bitstring);
        }
    }
    std::cerr<<"Mean compression for data of length "<<n<<" is "<<ratiosSum / counter<<"\n";
    return out;
}

void mainTest()
{
    std::cerr<<"mainTest\n";
    auto graph = exampleGraph();
    auto encoding = encode(graph);
    Bitstring properFirst{0,0,1,0,1,0,0,0,0,1,0,1,1,0,0,1};
    Bitstring properSecond{1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,0};
    assert(encoding.n == 10);
    assert(encoding.BFirst == properFirst);
    assert(encoding.BSecond == properSecond);
    assert(graphDecompression(encoding) == decompressedGraph());
    auto compression = compress(encoding);
    Bitstring properEncodedFirst{0,1,0,0,1,0,0,1,0,0,1,0,1,0,0,0,0,1};
    Bitstring properEncodedSecond{1,1,0,0,0,1,1,0,1,0,0,0,1,0,0,1,1};
    assert(compression.n == 10);
    assert(compression.BFirst.bits == properEncodedFirst);
    assert(compression.BFirst.length == 16);
    assert(compression.BSecond.bits == properEncodedSecond);
    assert(compression.BSecond.length == 17);
    assert(graphDecompression(compression) == decompressedGraph());
    std::cerr<<"\tOK!\n\n";
}

void roundingTest()
{
    std::cerr<<"roundingTest\n";
    assert(dimensionDependentRounding(2, 1) == 0);
    assert(dimensionDependentRounding(1, 1) == 0);
    assert(dimensionDependentRounding(0.5, 1) == 1);
    assert(dimensionDependentRounding(0.25, 1) == 2);
    assert(dimensionDependentRounding(1, 3) == 0);
    assert(dimensionDependentRounding(0.5, 3) == 0);
    assert(dimensionDependentRounding(0.25, 3) == 0);
    assert(dimensionDependentRounding(0.75, 3) == 0);
    assert(dimensionDependentRounding(0.125, 3) == 1);
    assert(dimensionDependentRounding(1./(1<<6), 3) == 2);
    std::cerr<<"\tOK!\n\n";
}

void cellsShouldHaveProperPositions(const ContinuousCell& cell)
{
    assert((cell.getPosition() == Position{0.,0.}));
    assert((cell.subCells[0]->getPosition() == Position{0.,0.}));
    assert((cell.subCells[0]->subCells[0]->getPosition() == Position{0.,0.}));
    assert((cell.subCells[0]->subCells[1]->getPosition() == Position{0.25,0.}));
    assert((cell.subCells[0]->subCells[2]->getPosition() == Position{0.,0.25}));
    assert((cell.subCells[0]->subCells[3]->getPosition() == Position{0.25,0.25}));
    assert((cell.subCells[1]->getPosition() == Position{0.5,0.}));
    assert((cell.subCells[1]->subCells[0]->getPosition() == Position{0.5,0.}));
    assert((cell.subCells[1]->subCells[1]->getPosition() == Position{0.75,0.}));
    assert((cell.subCells[1]->subCells[2]->getPosition() == Position{0.5,0.25}));
    assert((cell.subCells[1]->subCells[3]->getPosition() == Position{0.75,0.25}));
    assert((cell.subCells[2]->getPosition() == Position{0.,0.5}));
    assert((cell.subCells[2]->subCells[0]->getPosition() == Position{0.,0.5}));
    assert((cell.subCells[2]->subCells[1]->getPosition() == Position{0.25,0.5}));
    assert((cell.subCells[2]->subCells[2]->getPosition() == Position{0.,0.75}));
    assert((cell.subCells[2]->subCells[3]->getPosition() == Position{0.25,0.75}));
    assert((cell.subCells[3]->getPosition() == Position{0.5,0.5}));
    assert((cell.subCells[3]->subCells[0]->getPosition() == Position{0.5,0.5}));
    assert((cell.subCells[3]->subCells[1]->getPosition() == Position{0.75,0.5}));
    assert((cell.subCells[3]->subCells[2]->getPosition() == Position{0.5,0.75}));
    assert((cell.subCells[3]->subCells[3]->getPosition() == Position{0.75,0.75}));
}

void positionsShouldBeProperlyTranslatedIntoIndexes()
{
    std::cerr<<"positionsShouldBeProperlyTranslatedIntoIndexes\n";
    assert(positionToIndex(Position{0.  , 0.  }, 2) ==  0);
    assert(positionToIndex(Position{0.25, 0.  }, 2) ==  1);
    assert(positionToIndex(Position{0.5 , 0.  }, 2) ==  2);
    assert(positionToIndex(Position{0.75, 0.  }, 2) ==  3);
    assert(positionToIndex(Position{0.  , 0.25}, 2) ==  4);
    assert(positionToIndex(Position{0.25, 0.25}, 2) ==  5);
    assert(positionToIndex(Position{0.5 , 0.25}, 2) ==  6);
    assert(positionToIndex(Position{0.75, 0.25}, 2) ==  7);
    assert(positionToIndex(Position{0.  , 0.5 }, 2) ==  8);
    assert(positionToIndex(Position{0.25, 0.5 }, 2) ==  9);
    assert(positionToIndex(Position{0.5 , 0.5 }, 2) == 10);
    assert(positionToIndex(Position{0.75, 0.5 }, 2) == 11);
    assert(positionToIndex(Position{0.  , 0.75}, 2) == 12);
    assert(positionToIndex(Position{0.25, 0.75}, 2) == 13);
    assert(positionToIndex(Position{0.5 , 0.75}, 2) == 14);
    assert(positionToIndex(Position{0.75, 0.75}, 2) == 15);

    assert(positionToIndex(Position{0.  , 0.  }, 1) == 0);
    assert(positionToIndex(Position{0.25, 0.  }, 1) == 0);
    assert(positionToIndex(Position{0.  , 0.25}, 1) == 0);
    assert(positionToIndex(Position{0.25, 0.25}, 1) == 0);
    assert(positionToIndex(Position{0.5 , 0.  }, 1) == 1);
    assert(positionToIndex(Position{0.75, 0.  }, 1) == 1);
    assert(positionToIndex(Position{0.5 , 0.25}, 1) == 1);
    assert(positionToIndex(Position{0.75, 0.25}, 1) == 1);
    assert(positionToIndex(Position{0.  , 0.5 }, 1) == 2);
    assert(positionToIndex(Position{0.25, 0.5 }, 1) == 2);
    assert(positionToIndex(Position{0.  , 0.75}, 1) == 2);
    assert(positionToIndex(Position{0.25, 0.75}, 1) == 2);
    assert(positionToIndex(Position{0.5 , 0.5 }, 1) == 3);
    assert(positionToIndex(Position{0.75, 0.5 }, 1) == 3);
    assert(positionToIndex(Position{0.5 , 0.75}, 1) == 3);
    assert(positionToIndex(Position{0.75, 0.75}, 1) == 3);

    assert(positionToIndex(Position{0.  , 0.   + 0.15}, 2) ==  0);
    assert(positionToIndex(Position{0.25, 0.   + 0.15}, 2) ==  1);
    assert(positionToIndex(Position{0.5 , 0.   + 0.15}, 2) ==  2);
    assert(positionToIndex(Position{0.75, 0.   + 0.15}, 2) ==  3);
    assert(positionToIndex(Position{0.  , 0.25 + 0.15}, 2) ==  4);
    assert(positionToIndex(Position{0.25, 0.25 + 0.15}, 2) ==  5);
    assert(positionToIndex(Position{0.5 , 0.25 + 0.15}, 2) ==  6);
    assert(positionToIndex(Position{0.75, 0.25 + 0.15}, 2) ==  7);
    assert(positionToIndex(Position{0.  , 0.5  + 0.15}, 2) ==  8);
    assert(positionToIndex(Position{0.25, 0.5  + 0.15}, 2) ==  9);
    assert(positionToIndex(Position{0.5 , 0.5  + 0.15}, 2) == 10);
    assert(positionToIndex(Position{0.75, 0.5  + 0.15}, 2) == 11);
    assert(positionToIndex(Position{0.  , 0.75 + 0.15}, 2) == 12);
    assert(positionToIndex(Position{0.25, 0.75 + 0.15}, 2) == 13);
    assert(positionToIndex(Position{0.5 , 0.75 + 0.15}, 2) == 14);
    assert(positionToIndex(Position{0.75, 0.75 + 0.15}, 2) == 15);

    assert(positionToIndex(Position{0.   + 0.15, 0.  }, 2) ==  0);
    assert(positionToIndex(Position{0.25 + 0.15, 0.  }, 2) ==  1);
    assert(positionToIndex(Position{0.5  + 0.15, 0.  }, 2) ==  2);
    assert(positionToIndex(Position{0.75 + 0.15, 0.  }, 2) ==  3);
    assert(positionToIndex(Position{0.   + 0.15, 0.25}, 2) ==  4);
    assert(positionToIndex(Position{0.25 + 0.15, 0.25}, 2) ==  5);
    assert(positionToIndex(Position{0.5  + 0.15, 0.25}, 2) ==  6);
    assert(positionToIndex(Position{0.75 + 0.15, 0.25}, 2) ==  7);
    assert(positionToIndex(Position{0.   + 0.15, 0.5 }, 2) ==  8);
    assert(positionToIndex(Position{0.25 + 0.15, 0.5 }, 2) ==  9);
    assert(positionToIndex(Position{0.5  + 0.15, 0.5 }, 2) == 10);
    assert(positionToIndex(Position{0.75 + 0.15, 0.5 }, 2) == 11);
    assert(positionToIndex(Position{0.   + 0.15, 0.75}, 2) == 12);
    assert(positionToIndex(Position{0.25 + 0.15, 0.75}, 2) == 13);
    assert(positionToIndex(Position{0.5  + 0.15, 0.75}, 2) == 14);
    assert(positionToIndex(Position{0.75 + 0.15, 0.75}, 2) == 15);
    std::cerr<<"\tOK!\n\n";
}

void cellsShouldHaveProperLevels(const ContinuousCell& cell)
{
    assert((cell.getLevel() == 0));
    assert((cell.subCells[0]->getLevel() == 1));
    assert((cell.subCells[0]->subCells[0]->getLevel() == 2));
    assert((cell.subCells[0]->subCells[1]->getLevel() == 2));
    assert((cell.subCells[0]->subCells[2]->getLevel() == 2));
    assert((cell.subCells[0]->subCells[3]->getLevel() == 2));
    assert((cell.subCells[1]->getLevel() == 1));
    assert((cell.subCells[1]->subCells[0]->getLevel() == 2));
    assert((cell.subCells[1]->subCells[1]->getLevel() == 2));
    assert((cell.subCells[1]->subCells[2]->getLevel() == 2));
    assert((cell.subCells[1]->subCells[3]->getLevel() == 2));
    assert((cell.subCells[2]->getLevel() == 1));
    assert((cell.subCells[2]->subCells[0]->getLevel() == 2));
    assert((cell.subCells[2]->subCells[1]->getLevel() == 2));
    assert((cell.subCells[2]->subCells[2]->getLevel() == 2));
    assert((cell.subCells[2]->subCells[3]->getLevel() == 2));
    assert((cell.subCells[3]->getLevel() == 1));
    assert((cell.subCells[3]->subCells[0]->getLevel() == 2));
    assert((cell.subCells[3]->subCells[1]->getLevel() == 2));
    assert((cell.subCells[3]->subCells[2]->getLevel() == 2));
    assert((cell.subCells[3]->subCells[3]->getLevel() == 2));
}

void cellsShouldBeProperlyIterated(std::unique_ptr<ContinuousCell>& cell)
{
    auto iterator = VerticesInCellsGeometricOrdering::Iterator(cell.get());
    assert((cell.get() == (iterator++).get()));
    assert((cell->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[3].get() == (iterator++).get()));
    assert((iterator.get() == nullptr));
}

void cellsShouldBeProperlyIteratedOnOneLevel(std::unique_ptr<ContinuousCell>& cell)
{
    auto iterator = VerticesInCellsGeometricOrdering::LevelIterator(cell.get(), 1);
    assert((cell->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[3].get() == (iterator++).get()));
    assert((iterator.get() == nullptr));

    iterator = VerticesInCellsGeometricOrdering::LevelIterator(cell.get(), 2);
    assert((cell->subCells[0]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[0]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[1]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[2]->subCells[3].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[0].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[1].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[2].get() == (iterator++).get()));
    assert((cell->subCells[3]->subCells[3].get() == (iterator++).get()));
    assert((iterator.get() == nullptr));
}

void cellIteratorTest()
{
    std::cerr<<"cellIteratorTest\n";
    unsigned maxLevel = 2;
    unsigned dimension = 2;
    auto cells = generateCellsUpToGivenLevel(maxLevel, dimension);
    std::vector<Vertex> vertices;
    std::vector<std::vector<ContinuousCell*>> indexToCellMappings(maxLevel + 1);
    for(unsigned i = 0; i <= maxLevel; i++)
    {
        indexToCellMappings[i].resize(1u<<(dimension * i));
    }

    auto continuousCells = cells->generateContinuousOrdering(vertices, indexToCellMappings);

    cellsShouldHaveProperPositions(*continuousCells);
    cellsShouldHaveProperLevels(*continuousCells);
    cellsShouldBeProperlyIterated(continuousCells);
    cellsShouldBeProperlyIteratedOnOneLevel(continuousCells);

    std::cerr<<"\tOK!\n\n";
}

void verticesInCellsGeometricOrderingTest()
{
    std::cerr<<"verticesInCellsGeometricOrderingTest\n";
    std::vector<Position> positions = {
        {0.  , 0.  },
        {0.25, 0.  },
        {0.5 , 0.  },
        {0.75, 0.  },
        {0.  , 0.25},
        {0.25, 0.25},
        {0.5 , 0.25},
        {0.75, 0.25},
        {0.  , 0.5 },
        {0.25, 0.5 },
        {0.5 , 0.5 },
        {0.75, 0.5 },
        {0.  , 0.75},
        {0.25, 0.75},
        {0.5 , 0.75},
        {0.75, 0.75}
    };
    std::vector<Vertex> vertices;
    for(unsigned i=0;i<16;i++)
        vertices.push_back(Vertex{i,positions[i],16./i});
    VerticesInCellsGeometricOrdering verticesInCellsGeometricOrdering (vertices, 2, 2);

    std::vector<std::vector<int>> properOrderingIndexes = {
        { 0, 1, 4, 5, 2, 3, 6, 7, 8, 9, 12, 13, 10, 11, 14, 15 },
        { 0, 1, 4, 5 },
        { 0 },
        { 1 },
        { 4 },
        { 5 },
        { 2, 3, 6, 7 },
        { 2 },
        { 3 },
        { 6 },
        { 7 },
        { 8, 9, 12, 13 },
        { 8 },
        { 9 },
        { 12 },
        { 13 },
        { 10, 11, 14, 15 },
        { 10 },
        { 11 },
        { 14 },
        { 15 }
    };

    int orderingIndex = 0;
    for(auto& cell : verticesInCellsGeometricOrdering )
    {
        int vertexInOrderingIndex = 0;
        for(Vertex& vertex : cell)
        {
            assert(vertex.index == properOrderingIndexes[orderingIndex][vertexInOrderingIndex]);
            ++vertexInOrderingIndex;
        }
        ++orderingIndex;
    }

    std::cerr<<"\tOK!\n\n";
}

void partitionTest()
{

}

void singleNeighboursCellsTest(unsigned index, unsigned level, unsigned dimension, std::vector<unsigned> properOutput)
{
    auto neighbours = getNeighbourIndexes(index, level, dimension);
    assert(neighbours.size() == properOutput.size());
    for(unsigned i = 0; i < neighbours.size(); i++)
        assert(neighbours[i] == properOutput[i]);
}

void neighbourCellsTest()
{
    std::cerr<<"neighbourCellsTest\n";
    singleNeighboursCellsTest(3,  1, 2, {3, 0, 1, 2});
    singleNeighboursCellsTest(5,  2, 2, {5, 4, 6, 1, 9, 0, 8, 2, 10});
    singleNeighboursCellsTest(0,  2, 2, {0, 3, 1, 12, 4, 15, 7, 13, 5});
    singleNeighboursCellsTest(15, 2, 2, {15, 14, 12, 11, 3, 10, 2, 8, 0});
    singleNeighboursCellsTest(5,  2, 3, {5, 4, 6, 1, 9, 0, 8, 2, 10, 53, 21, 52, 20, 54, 22, 49, 17, 57, 25, 48, 16, 56, 24, 50, 18, 58, 26});
    singleNeighboursCellsTest(0,  2, 3, {0, 3, 1, 12, 4, 15, 7, 13, 5, 48, 16, 51, 19, 49, 17, 60, 28, 52, 20, 63, 31, 55, 23, 61, 29, 53, 21});

    std::cerr<<"\tOK!\n\n";
}






int main()
{
    roundingTest();
    binaryArithmeticCodingTests(10);
    mainTest();
    positionsShouldBeProperlyTranslatedIntoIndexes();
    cellIteratorTest();
    partitionTest();
    verticesInCellsGeometricOrderingTest();
    neighbourCellsTest();
}
