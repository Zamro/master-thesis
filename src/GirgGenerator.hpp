#pragma once
#include <random>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <functional>
#include <memory>
#include <iostream>
#include <utility>
#include "Graph.hpp"
#include "Vertex.hpp"
#include "GirgCells.hpp"
#include "Layering.hpp"
#include "VerticesInCellsGeometricOrdering.hpp"

class GeometricDistributionOverflow: public std::exception
{
    virtual const char* what() const throw()
    {
        return "My exception happened";
    }
};

class UniformToGeometric{
    ValueType denominator;
public:
    UniformToGeometric(ValueType p):
            denominator(std::log(1-p))
    {}
    unsigned operator()(ValueType uniform){
        ValueType value = std::ceil(std::log(uniform)/denominator);
        if (value > nextafter(std::numeric_limits<unsigned>::max(), 0))
            throw GeometricDistributionOverflow{};
        return unsigned(value);
    }
};

class GirgGenerator{
    static std::vector<VerticesInCellsGeometricOrdering> generateGeometricOrderings(unsigned dimension,const  Layering& layering)
    {
        std::vector<VerticesInCellsGeometricOrdering> orderings;
        for(int i = 0; i < layering.layers.size(); i++)
        {
            double wi = layering.limits[i+1];
            double minimalCellSize = wi * layering.wmin / layering.sumOfWeights;
            auto maximumCellsLevel = dimensionDependentRounding(minimalCellSize, dimension);
            orderings.push_back({layering.layers[i], dimension, maximumCellsLevel});
        }
        return orderings;
    }
public:
    static Graph generate(unsigned n, unsigned dimension, ValueType alpha, ValueType beta, ValueType delta)
    {
        Graph out{n};

        std::vector<Vertex> vertices;
        for(unsigned i=0; i<n; i++)
            vertices.push_back(Vertex(i, n, dimension, beta, delta));
        Layering layering{vertices};
        auto verticesInCellsGeometricOrderings = generateGeometricOrderings(dimension, layering);
        for(int i = 0; i < layering.layers.size(); i++)
            for(int j = i; j < layering.layers.size(); j++)
            {
                double wi = layering.limits[i+1];
                double wj = layering.limits[j+1];
                double normalizedWeight = wi * wj / layering.sumOfWeights;
                auto maximumCellsLevel = dimensionDependentRounding(normalizedWeight, dimension);
                for(ContinuousCell* a : verticesInCellsGeometricOrderings[i].getCellsAtLevel(maximumCellsLevel))
                    for(ContinuousCell* b : verticesInCellsGeometricOrderings[j].getNeighbourCells(a->getPosition(), maximumCellsLevel))
                        for(Vertex& u : *a)
                            for(Vertex& v : *b)
                                if(i != j || u.index < v.index)
                                    if(tossIfShouldBeConnected(u, v, layering.sumOfWeights, alpha))
                                        out.putEdge(u.index, v.index);

                for(unsigned level = 0; level + 1 < maximumCellsLevel ; level++)
                    for(ContinuousCell* aParent : verticesInCellsGeometricOrderings[i].getCellsAtLevel(level))
                        for(ContinuousCell* bParent : verticesInCellsGeometricOrderings[j].getNeighbourCells(aParent->getPosition(), aParent->getLevel()))
                            for(ContinuousCell& a : aParent->subCells)
                                for(ContinuousCell& b : bParent->subCells)
                                {
                                    ValueType  distance = a.distanceFrom(b);
                                    if(distance > 0.)
                                    {
                                        auto normalizedDistance = std::pow(distance, dimension);
                                        ValueType meanProbability = std::min(1.0,
                                                                             std::pow(normalizedWeight / normalizedDistance,
                                                                                      alpha));
                                        auto numberOfPairs = a.size() * b.size();
                                        UniformToGeometric uniformToGeometric(meanProbability);
                                        try{
                                            for(unsigned r = uniformToGeometric(generator()) - 1; r < numberOfPairs; r += uniformToGeometric(generator()))
                                            {
                                                Vertex& u = a[r / b.size()];
                                                Vertex& v = b[r % b.size()];
                                                if(i != j || u.index < v.index)
                                                {
                                                    if(tossIfShouldBeConnected(u, v, layering.sumOfWeights, alpha, meanProbability))
                                                        out.putEdge(u.index, v.index);
                                                }
                                            }
                                        }catch(GeometricDistributionOverflow& g){}
                                    }
                                }
            }

        return out;
    }
};
