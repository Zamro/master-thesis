#pragma once
#include "BinaryEncoding.hpp"
#include <cmath>

constexpr unsigned long NUM_SIGNIFICANT_BITS = 11;
constexpr double MIN_PROB = ldexp(1., -((long)NUM_SIGNIFICANT_BITS - 1));

class Value{
    double value{};
    Bitstring::const_iterator currentPosition;
    Bitstring::const_iterator endPosition;
public:
    Value(const Bitstring&);
    double& get();
    void readNextBit(double offset);
};

struct Interval{
	double base = 0.;
	double length = 1.;

	void update(bool shouldChooseSecondSubinterval, double firstSubintervalLength);
    Bitstring encodingRenormalize();
    Value decodingRenormalize(Value& v);
};

class BinaryArithmeticCoding{
	Interval interval{};
	int C[2] = {1, 2};

	void updateInterval(bool s);
	bool selectInterval(double v);
    void updateDistribution(bool s);
	void propagateCarry(Bitstring& output);
    void finalize(Bitstring& output);
public:
    BinaryEncoding encode(const Bitstring& input);
    Bitstring decode(const BinaryEncoding& input);
};
