#pragma once
#include <vector>
#include <memory>
#include <list>
#include "Vertex.hpp"


class ContinuousCell
{
private:
    ContinuousCell* parent;
    unsigned long indexInParentArray;
    Position position;
    unsigned level;
    std::vector<Vertex>::iterator beginIt;
    std::vector<Vertex>::iterator endIt;
public:
    std::vector<ContinuousCell> subCells{};
    ContinuousCell(unsigned maxLevel,
                   unsigned dimension,
                   Position position = {},
                   unsigned currentLevel = 0,
                   ContinuousCell* parent = nullptr);
    void setVertices(std::vector<Vertex> &target,
                     std::vector<std::vector<ContinuousCell*>>& indexToCellMappings,
                     std::vector<std::list<Vertex>>& temporaryCells);

    ContinuousCell* getNext(long indexOfLastSubCell = -1){
        if(subCells.size() > indexOfLastSubCell + 1)
        {
            return &subCells[indexOfLastSubCell + 1];
        }
        else if (parent == nullptr)
        {
            return nullptr;
        }
        else
        {
            return parent->getNext(indexInParentArray);
        }
    };

    unsigned getLevel() const {return level;}
    ValueType getEdgeLength() const {return std::ldexp(1.0, -int(level));} // 2^-level
    ContinuousCell& getParent() {return *parent;}
    const ContinuousCell& getParent() const {return *parent;}
    const Position getPosition() const {return position;}
    Position getPosition() {return position;}
    std::vector<Vertex>::iterator begin(){return beginIt;}
    std::vector<Vertex>::iterator end(){return endIt;}
    const std::vector<Vertex>::iterator begin() const {return beginIt;}
    const std::vector<Vertex>::iterator end() const {return endIt;}
    void setEnd(std::vector<Vertex>::iterator end){
        endIt = end;
    }
    ValueType distanceFrom(const ContinuousCell& other)
    {
        auto myPosition = position;
        auto otherPosition = other.position;
        ValueType cellEdgeLength = getEdgeLength();
        for(unsigned i = 0; i< myPosition.size(); i++)
        {
            if(myPosition[i] != otherPosition[i])
            {
                if(distanceOnCircle(std::fmod(myPosition[i] + cellEdgeLength,1.0), otherPosition[i]) <
                   distanceOnCircle(std::fmod(otherPosition[i] + cellEdgeLength,1.0), myPosition[i]))
                    myPosition[i] = std::fmod(myPosition[i] + cellEdgeLength,1.0);
                else
                    otherPosition[i] = std::fmod(otherPosition[i] + cellEdgeLength,1.0);
            }
        }
        return distanceOnTorus(myPosition, otherPosition);
    }
    auto size(){return endIt - beginIt;}
    Vertex& operator[](unsigned index){return *(beginIt + index);}
};
