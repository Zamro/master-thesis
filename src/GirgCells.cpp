//
// Created by Pawel on 23.04.2017.
//
#include <iostream>
#include "GirgCells.hpp"

ContinuousCell::ContinuousCell(unsigned maxLevel,
               unsigned dimension,
               Position position,
               unsigned currentLevel,
               ContinuousCell* parent) :
        parent(parent),
        position(position),
        level(currentLevel)
{
    if(position.empty())
        position = Position(dimension, 0.);
    if(maxLevel - currentLevel > 0)
    {
        ValueType cellEdgeLength = std::ldexp(0.5, -int(currentLevel));
        Position newPosition;
        newPosition.resize(dimension);
        for(unsigned i=0; i<(1<<dimension); i++)
        {
            for(unsigned bitCount = 0; bitCount < dimension; bitCount++)
                newPosition[bitCount] = (position[bitCount] + (i & (1 << bitCount) ? cellEdgeLength : 0));
            subCells.emplace_back(maxLevel, dimension, newPosition, currentLevel + 1, this);
        }
    }
}

void ContinuousCell::setVertices(std::vector<Vertex> &target,
                                 std::vector<std::vector<ContinuousCell*>>& indexToCellMappings,
                                 std::vector<std::list<Vertex>>& temporaryCells)
{
    indexToCellMappings[level][positionToIndex(position, level)] = this;
    beginIt = target.end();
    if(subCells.empty())
    {
        auto& vertices = temporaryCells[positionToIndex(position, level)];
        target.insert(target.end(), std::move(vertices.begin()), std::move(vertices.end()));
    }
    for(auto& cell : subCells)
    {
        cell.setVertices(target, indexToCellMappings, temporaryCells);
    }
    endIt = target.end();
}
