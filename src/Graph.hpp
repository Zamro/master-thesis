#pragma once
#include <vector>
#include <ostream>

struct Node
{
    unsigned id;
	std::vector<unsigned> neighboursList;
	
    bool operator<(const Node& node) const
    {
        return id<node.id;
    }
};

class Graph
{
	friend std::ostream& operator<<(std::ostream&, const Graph&);
public:
	Graph(unsigned n);
	virtual ~Graph() = default;
	bool operator==(const Graph& other);
	void putEdge(int i, int j);
	Node& node(int i);
	const Node& node(int i) const;
	unsigned n;
	std::vector<Node> nodes;
	unsigned getNumberOfEdges() const;
};

class MathematicaGraphWriter
{
public:
	Graph& graph;
	MathematicaGraphWriter(Graph graph):graph(graph){}
};

std::ostream& operator<<(std::ostream&, const Graph& graph);
std::ostream& operator<<(std::ostream&, const MathematicaGraphWriter& graph);