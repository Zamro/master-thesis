#include <algorithm>
#include <iostream>
#include "BinaryArithmeticCoding.hpp"

double myFloor(double a)
{
    return std::ldexp(floor(std::ldexp(a, NUM_SIGNIFICANT_BITS)), -(long)NUM_SIGNIFICANT_BITS);
}
double myCeil(double a)
{
    return std::ldexp(ceil(std::ldexp(a, NUM_SIGNIFICANT_BITS)), -(long)NUM_SIGNIFICANT_BITS);
}

void Interval::update(bool shouldChooseSecondSubinterval, double firstSubintervalLength)
{
    if(shouldChooseSecondSubinterval)
    {
        base = base + firstSubintervalLength;
        length = length - firstSubintervalLength;
    }
    else
        length = firstSubintervalLength;

    base = myCeil(base);
    length = myFloor(length);

}

Bitstring Interval::encodingRenormalize()
{
    Bitstring output;
    while(length <= 0.5)
    {
        bool isOverflow = (base >= 0.5);
        output.push_back(isOverflow);
        base = std::ldexp(base - isOverflow * 0.5, 1);
        length = std::ldexp(length, 1);
    }
    base = myCeil(base);
    length = myFloor(length);
    return output;
}

Value Interval::decodingRenormalize(Value& v)
{
    while(length <= 0.5)
    {
        double offset = (base >= 0.5) ? 0.5 : 0;
        base = std::ldexp(base - offset, 1);
        length = std::ldexp(length, 1);
        v.readNextBit(offset);
    }
    base = myCeil(base);
    length = myFloor(length);
    return v;
}

void merge(Bitstring& out, Bitstring&& in)
{
	out.insert(out.end(),
			   std::make_move_iterator(in.begin()),
			   std::make_move_iterator(in.end())); 
}

BinaryEncoding BinaryArithmeticCoding::encode(const Bitstring& input)
{
    Bitstring output{};
    for(bool s : input)
    {
        updateInterval(s);
         if(interval.base >= 1)
         {
             interval.base -= 1;
             propagateCarry(output);
         }
        merge(output, interval.encodingRenormalize());
        updateDistribution(s);
    }
    finalize(output);
    return {output, input.size()};
}

Bitstring BinaryArithmeticCoding::decode(const BinaryEncoding& input)
{
    Bitstring output;
    Value v{input.bits};
    while(output.size() != input.length)
    {
        bool s = selectInterval(v.get());
        output.push_back(s);
        if(interval.base >= 1.)
        {
            interval.base -= 1.;
            v.get() -= 1.;
        }
        v = interval.decodingRenormalize(v);
        updateDistribution(s);
    }
    return output;
}

void BinaryArithmeticCoding::updateInterval(bool s)
{
    double probability = C[0]/double(C[1]);
    if(probability < MIN_PROB || 1. - probability < MIN_PROB)
        throw std::underflow_error{"Probability to small for given number of significant bits during encoding."};
    double firstSubintervalLength = interval.length * probability;
	interval.update(s, firstSubintervalLength);
}

bool BinaryArithmeticCoding::selectInterval(double v)
{
    double probability = C[0]/double(C[1]);
    if(probability < MIN_PROB || 1. - probability < MIN_PROB)
        throw std::underflow_error{"Probability to small for given number of significant bits during decoding."};
    double firstSubintervalLength = interval.length * probability;
	bool s = (interval.base  + firstSubintervalLength <= v);
	interval.update(s, firstSubintervalLength);
	return s;
}

void BinaryArithmeticCoding::updateDistribution(bool s)
{
	if(s == 0) C[0]++;
    C[1]++;
}

void BinaryArithmeticCoding::propagateCarry(Bitstring& output)
{
    auto iter = output.rbegin();
    while(iter != output.rend() && *iter)
        *(iter++) = 0;
    if(iter != output.rend())
        *iter = 1;
    else
        std::cerr<<"ERROR: code overflowed 1.\n";
}

void BinaryArithmeticCoding::finalize(Bitstring& output) //Code_Value_Selection
{
    if(interval.base <= 0.5)
        output.push_back(1);
    else
        propagateCarry(output);
}

Value::Value(const Bitstring& bitstring):
        currentPosition{bitstring.begin()},
        endPosition{bitstring.end()}
{
    unsigned long bitsToRead = std::min(NUM_SIGNIFICANT_BITS, bitstring.size());
    for(long i = bitsToRead - 1; i>=0; i--)
    {
        value += bitstring[i];
        value = std::ldexp(value, -1);
    }
    currentPosition += bitsToRead;
}

double& Value::get() {
    return value;
}

void Value::readNextBit(double offset)
{
    value = std::ldexp(value - offset, 1);
//    std::cerr<<"readNextBit: currentPosition != endPosition "<<(currentPosition != endPosition)<<"\n";
    if(currentPosition != endPosition)
    {
        value += ldexp(*(currentPosition++), -(long)NUM_SIGNIFICANT_BITS);
    }
}
