#pragma once
#include <vector>
#include <random>
#include <chrono>

template<class GraphType>
GraphType generateGnp(int n, double p){
	static std::default_random_engine generator{std::chrono::system_clock::now().time_since_epoch().count()};
	static std::uniform_real_distribution<double> dist(0., 1.);
	GraphType graph(n);
	for(int i = 0; i<n; i++)
		for(int j = i + 1 ; j<n; j++)
		{
			double random = dist(generator);
			if(random < p)
				graph.putEdge(i,j);
		}
	return graph;
}
