//
// Created by Pawel on 23.04.2017.
//

#pragma once
#include <random>
#include <chrono>
#include <algorithm>
#include <functional>
#include <memory>
#include <iostream>
#include "Vertex.hpp"

class Layering
{
public:
    std::vector<std::vector<Vertex>> layers;
    std::vector<double> limits;
    double wmin;
    double wmax;
    double sumOfWeights;
    Layering(std::vector<Vertex>& vertices);
};

