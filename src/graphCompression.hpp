#pragma once
#include "BinaryEncoding.hpp"
#include "Graph.hpp"

struct BitEncoding
{
    int n;
    Bitstring BFirst;
    Bitstring BSecond;
    void addValue(unsigned n, unsigned length);
};

struct CompressedBitEncoding{
    int n;
    BinaryEncoding BFirst;
    BinaryEncoding BSecond;
    unsigned getNumberOfBits(){
            return 8*(5 * sizeof(int)) //n, BFirst.length, BSecond.length, lengthof BFirst.bits, lengthof BSecond.bits
                   + BFirst.bits.size()
                   + BSecond.bits.size();
    }
};

class BitExtractor
{
    Bitstring::const_iterator bFirstIt;
    Bitstring::const_iterator bSecondIt;
public:
    BitExtractor(const BitEncoding& encoding):
            bFirstIt{encoding.BFirst.begin()},
            bSecondIt{encoding.BSecond.begin()}
    {}
    unsigned get(unsigned length);
};

BitEncoding encode(Graph &g);
BitEncoding girgEncode(Graph &g);
CompressedBitEncoding compress(const BitEncoding &g);
CompressedBitEncoding compress(Graph &g);
CompressedBitEncoding girgCompress(Graph &g);
BitEncoding decompress(const CompressedBitEncoding& bitEncoding);
Graph graphDecompression(const BitEncoding& bitEncoding);
Graph graphDecompression(const CompressedBitEncoding& bitEncoding);
