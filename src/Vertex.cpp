#include "Vertex.hpp"

std::uniform_real_distribution<ValueType> Vertex::dist {0., 1.};
std::default_random_engine generator {(unsigned long)std::chrono::system_clock::now().time_since_epoch().count()};

Vertex::Vertex(unsigned i, unsigned n, unsigned d, double beta, double delta) :
    index(i)
{
    x.reserve(d);
    for(unsigned j = 0; j<d; j++)
    {
        x.push_back(dist(generator));
    }
    weight = delta * std::pow(n / ValueType(i + 1), 1./(beta-1.));
}

Vertex::Vertex(unsigned index, Position x, ValueType weight):
        index(index),
        x(x),
        weight(weight)
{

}

unsigned dimensionDependentRounding(ValueType x, unsigned dimension) {
    return (unsigned)std::max(0., std::floor(-std::log2(x)/dimension));
}

bool tossCoinWithGivenProbability(ValueType p) {
    return std::bernoulli_distribution(p)(generator);
}

ValueType distanceOnCircle(ValueType x, ValueType y) {
    ValueType euclideanDistance = std::abs(x-y);
    return std::min(euclideanDistance, 1 - euclideanDistance);
}

//We use infinity-metric, so we choose distance in the direction in which it is longest.
ValueType distanceOnTorus(const Position& x, const Position& y) {
    ValueType distance = 0;
    for(unsigned index = 0; index < x.size(); index++)
    {
        ValueType newDistance = distanceOnCircle(x[index], y[index]);
        if(newDistance > distance)
            distance = newDistance;
    }
    return distance;
}

ValueType probabilityOfConnecting(const Vertex &u, const Vertex &v, ValueType totalWeight, ValueType alpha){
    auto dimension = u.x.size();
    auto normalizedWeight = u.weight*v.weight/totalWeight;
    auto normalizedDistance = std::pow(distanceOnTorus(u.x, v.x), dimension);
    return std::min(1.0, std::pow(normalizedWeight/normalizedDistance, alpha));
}

bool tossIfShouldBeConnected(const Vertex &u, const Vertex &v, ValueType totalWeight, ValueType alpha, ValueType probabilityDenominator){
    ValueType probability = probabilityOfConnecting(u, v, totalWeight, alpha);
    return tossCoinWithGivenProbability(probability / probabilityDenominator);
}

void writePosition(const Position &pos) {
    for(auto p : pos)
        std::cerr<<p<<" ";
    std::cerr<<"\n";
}

unsigned positionToIndex(const Position& position, unsigned cellLevel){
    unsigned multiplier = 1u;
    unsigned index = 0;
    for(auto pos : position)
    {
        unsigned singleDimensionIndex = unsigned(std::ldexp(pos, int(cellLevel)));
        index += singleDimensionIndex * multiplier;
        multiplier <<= cellLevel;
    }
    return index;
}

std::vector<unsigned> getNeighbourIndexes(unsigned index, unsigned cellLevel, unsigned maxDimension)
{
    std::vector<unsigned> out = {index};
    if(cellLevel == 1)
    {
        for(unsigned i = 0; i < 1u << maxDimension; i++)
            if(out.front() != i)out.push_back(i);
    }
    else if( cellLevel > 1)
    {
        unsigned rowLength = 1u << cellLevel;
        for(unsigned dimension = 0; dimension < maxDimension; dimension++)
        {
            unsigned long length = out.size();
            for(unsigned long i = 0; i < length; i++)
            {
                unsigned distanceInCurrentDirection = 1u << (cellLevel * dimension);
                unsigned positionInCurrentDimension = (out[i]  / distanceInCurrentDirection) % rowLength;
                out.push_back(out[i] + distanceInCurrentDirection*(
                        (positionInCurrentDimension + rowLength - 1)%rowLength - positionInCurrentDimension
                ));
                out.push_back(out[i] + distanceInCurrentDirection*(
                        (positionInCurrentDimension + 1)%rowLength - positionInCurrentDimension
                ));
            }
        }
    }
    return out;
}