#include <iostream>
#include <algorithm>
#include <cassert>
#include <list>
#include "graphCompression.hpp"
#include "BinaryArithmeticCoding.hpp"

using Partition = std::list<std::list<unsigned>>;

Partition makePartition(int n)
{
	Partition partition;
	partition.push_back({});
	for(int i=0; i<n; i++)
		partition.front().push_back(i);
	return partition;
}

std::list<unsigned> splitPartition(std::list<unsigned>& vertices, const std::vector<unsigned>& neighbours)
{
    std::list<unsigned> out;
    for(auto it = vertices.begin(); it != vertices.end(); it++)
        if(std::binary_search(neighbours.begin(), neighbours.end(), *it))
        {
            //splice transfers element it from vertices into out at out.end()
            auto tempIt = it;
            tempIt--;
            out.splice(out.end(), vertices, it);
            it = tempIt;
        }
    return out;
}

std::list<unsigned> splitPartition(std::list<unsigned>& vertices, unsigned n)
{
    std::list<unsigned> out;
    assert(n <= vertices.size());
    auto end = vertices.begin();
    for(unsigned i =0; i< n; i++)
        end++;
    out.splice(out.end(), vertices, vertices.begin(), end);
    return out;
}

void sortNeighbours(Graph& g)
{
    for(auto& node :g.nodes)
        std::sort(node.neighboursList.begin(), node.neighboursList.end());
}

unsigned numberOfBits(unsigned n)
{
    unsigned out{};
    while(n)
    {
        out++;
        n >>=1;
    }
    return out;
}

void BitEncoding::addValue(unsigned n, unsigned biggestValueToFit)
{
    if(biggestValueToFit == 1)
        BSecond.push_back((bool)n);
    else
    {
        int mask = 1 << (numberOfBits(biggestValueToFit) - 1);
        while(mask != 0)
        {
            BFirst.push_back((bool)(n & mask));
            mask >>= 1;
        }
    }
}

unsigned BitExtractor::get(unsigned biggestValueToFit)
{
    unsigned out{};
    if(biggestValueToFit == 1)
        out = *(bSecondIt++) ? 1 : 0;
    else
    {
        for(unsigned i = 0; i < numberOfBits(biggestValueToFit); i++)
        {
            out <<= 1;
            if(*(bFirstIt++)) out |= 0x1;
        }
    }
    return out;
}

BitEncoding encode(Graph &g)
{
    sortNeighbours(g);
    BitEncoding encoding;
    encoding.n = g.n;
	Partition partition = makePartition(g.n);
	while(not partition.empty())
	{
        if(partition.front().empty())
            partition.pop_front();
        else
        {
            int v = partition.front().front();
            partition.front().pop_front();
            for(auto it = partition.begin(); it != partition.end(); it++)
            {
                if(not it->empty())
                {
                    auto numberOfNodes = it->size();
                    auto it2 = partition.insert(it, splitPartition(*it, g.nodes[v].neighboursList));
                    auto numberOfNeighbours = it2->size();
                    encoding.addValue(numberOfNeighbours, numberOfNodes);
                }
            }
        }
	}

    return encoding;
}

Graph graphDecompression(const BitEncoding& encoding)
{
    Graph graph(encoding.n);
    Partition partition = makePartition(encoding.n);
    BitExtractor extractor{encoding};
    while(not partition.empty())
    {
        if(partition.front().empty())
            partition.pop_front();
        else
        {
            int v = partition.front().front();
            partition.front().pop_front();
            for(auto it = partition.begin(); it != partition.end(); it++)
            {
                if(not it->empty())
                {
                    auto numberOfNodes = it->size();
                    int numberOfNeighbours = extractor.get(numberOfNodes);
                    auto newSet = partition.insert(it, splitPartition(*it, numberOfNeighbours));
                    for(int vertex : *newSet)
                    {
                        graph.putEdge(v, vertex);
                    }
                }
            }
        }
    }
    return graph;
}

BitEncoding decompress(const CompressedBitEncoding& bitEncoding)
{
    return {bitEncoding.n,
            BinaryArithmeticCoding().decode(bitEncoding.BFirst),
            BinaryArithmeticCoding().decode(bitEncoding.BSecond)};
}

Graph graphDecompression(const CompressedBitEncoding& compressedEncoding)
{
    return graphDecompression(decompress(compressedEncoding));
}

CompressedBitEncoding compress(const BitEncoding &bitEncoding) {
    return {bitEncoding.n,
            BinaryArithmeticCoding().encode(bitEncoding.BFirst),
            BinaryArithmeticCoding().encode(bitEncoding.BSecond)};
}

CompressedBitEncoding compress(Graph &g) {
    return compress(encode(g));
}

CompressedBitEncoding girgCompress(Graph &g) {
    return compress(girgEncode(g));
}

void writeUnsignedLongToBitVector(unsigned long a, std::vector<bool>& v)
{
    for(int i =0; i<sizeof(unsigned long); i++) {
        v.push_back(bool(a & 1));
        a>>=1;
    }
}

BitEncoding girgEncode(Graph &g) {
    sortNeighbours(g);
    BitEncoding encoding;
    encoding.n = g.n;
    for(const auto& node : g.nodes)
    {
        writeUnsignedLongToBitVector(node.neighboursList.size(), encoding.BFirst);
        for(auto neighbour : node.neighboursList)
            if(neighbour < node.id) writeUnsignedLongToBitVector(neighbour, encoding.BFirst);
    }
    return encoding;}
